package br.com.squamata.spending.service.model.v1;

import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.model.User;
import br.com.squamata.spending.model.v1.mapper.UserMapper;
import br.com.squamata.spending.repository.UserRepository;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public class UserDomainModelTest extends TestSupport {

    @Mock
    private UserRepository userRepository;
    @Mock
    private UserMapper userMapper;
    private String id;
    private User user;
    private UserDomainModel userDomainModel;
    private Date insertDate;

    @Before
    public void setUp() {
        insertDate = new Date();
        id = UUID.randomUUID().toString();
        user = expectedUser();
        userDomainModel = createUserDomainModel();
        userDomainModel.setUserRepository(userRepository);
        userDomainModel.setUserMapper(userMapper);
    }

    @Test
    public void testPersist() {
        when(userRepository.save(user)).thenReturn(user);
        when(userMapper.mapModel(userDomainModel)).thenReturn(user);
        when(userMapper.mapDomainModel(user)).thenReturn(userDomainModel);

        userDomainModel.persist();
        InOrder inOrder = inOrder(userMapper, userRepository);
        inOrder.verify(userMapper).mapModel(userDomainModel);
        inOrder.verify(userRepository).save(user);
        inOrder.verify(userMapper).mapDomainModel(user);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testFetchaByUserName() {
        when(userRepository.findByUserName(userDomainModel.getUserName())).thenReturn(user);
        userDomainModel.fetchByUserName();
        InOrder inOrder = inOrder(userRepository, userMapper);
        inOrder.verify(userRepository).findByUserName(userDomainModel.getUserName());
        inOrder.verify(userMapper).mapDomainModel(user);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testFetchById() {
        when(userRepository.findOne(userDomainModel.getId())).thenReturn(user);
        userDomainModel.fetchById();
        InOrder inOrder = inOrder(userRepository, userMapper);
        inOrder.verify(userRepository).findOne(userDomainModel.getId());
        inOrder.verify(userMapper).mapDomainModel(user);
        inOrder.verifyNoMoreInteractions();
    }

    private UserDomainModel createUserDomainModel() {
        UserDomainModel userDomainModel = new UserDomainModel();
        userDomainModel.setId(id);
        userDomainModel.setName("Felipe Adorno");
        userDomainModel.setPhone("(11)96403-3391");
        userDomainModel.setUserName("felipeadsc@gmail.com");
        userDomainModel.setPassword("123456");
        userDomainModel.setEnabled(Boolean.TRUE);
        userDomainModel.setInsertDate(insertDate);
        return userDomainModel;
    }

    private User expectedUser() {
        User user = new User();
        user.setId(id);
        user.setUserName("felipeadsc@gmail.com");
        user.setAccountNonExpired(Boolean.TRUE);
        user.setAccountNonLocked(Boolean.TRUE);
        user.setCredentialsNonExpired(Boolean.TRUE);
        user.setEnabled(Boolean.TRUE);
        user.setName("Felipe Adorno");
        user.setPassword("123456");
        user.setPhone("(11)96403-3391");
        List<Role> roles = new ArrayList<Role>();
        roles.add(Role.ROLE_USER);
        user.setRoles(roles);
        user.setInsertDate(insertDate);
        return user;
    }

}
