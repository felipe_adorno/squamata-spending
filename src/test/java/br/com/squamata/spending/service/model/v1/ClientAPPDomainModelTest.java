package br.com.squamata.spending.service.model.v1;

import br.com.squamata.spending.model.AuthorizedGrantType;
import br.com.squamata.spending.model.ClientAPP;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.model.v1.mapper.ClientAPPMapper;
import br.com.squamata.spending.repository.ClientAPPRepository;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class ClientAPPDomainModelTest.
 *
 * @author cad_fadorno
 */
public class ClientAPPDomainModelTest extends TestSupport {

    @Mock
    private ClientAPPRepository clientAPPRepository;
    @Mock
    private ClientAPPMapper clientAPPMapper;
    private ClientAPPDomainModel clientAPPDomainModel;
    private ClientAPP clientAPP;

    @Before
    public void setup() {
        clientAPP = expectedClientAPP();
        clientAPPDomainModel = expectedClientAPPDomainModel();
        clientAPPDomainModel.setClientAPPMapper(clientAPPMapper);
        clientAPPDomainModel.setClientAPPRepository(clientAPPRepository);
    }

    @Test
    public void testPersist() {
        when(clientAPPRepository.save(clientAPP)).thenReturn(clientAPP);
        when(clientAPPMapper.mapDomainModel(clientAPP)).thenReturn(clientAPPDomainModel);
        when(clientAPPMapper.mapModel(clientAPPDomainModel)).thenReturn(clientAPP);

        clientAPPDomainModel.persist();

        InOrder inOrder = inOrder(clientAPPRepository, clientAPPMapper);
        inOrder.verify(clientAPPMapper).mapModel(clientAPPDomainModel);
        inOrder.verify(clientAPPRepository).save(clientAPP);
        inOrder.verify(clientAPPMapper).mapDomainModel(clientAPP);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void testFetchById() {
        when(clientAPPRepository.findOne(clientAPPDomainModel.getId())).thenReturn(clientAPP);

        clientAPPDomainModel.fetchById();

        InOrder inOrder = inOrder(clientAPPRepository, clientAPPMapper);
        inOrder.verify(clientAPPRepository).findOne(clientAPPDomainModel.getId());
        inOrder.verify(clientAPPMapper).mapDomainModel(clientAPP);
        inOrder.verifyNoMoreInteractions();
    }

    private ClientAPP expectedClientAPP() {
        ClientAPP clientAPP = new ClientAPP();
        clientAPP.setClientSecret("123");
        clientAPP.setId("123");
        clientAPP.setAppName("Squamata-app");

        List<String> authorizedGrantTypes = new ArrayList<String>();
        authorizedGrantTypes.add(AuthorizedGrantType.PASSWORD.getAuthorizedGrantType());
        clientAPP.setAuthorizedGrantTypes(authorizedGrantTypes);

        List<Role> roles = new ArrayList<Role>();
        roles.add(Role.ROLE_USER);
        clientAPP.setRoles(roles);

        return clientAPP;
    }

    private ClientAPPDomainModel expectedClientAPPDomainModel() {
        ClientAPPDomainModel clientAPPDomainModel = new ClientAPPDomainModel("Squamata-app");
        clientAPPDomainModel.setId("123");
        return clientAPPDomainModel;
    }
}
