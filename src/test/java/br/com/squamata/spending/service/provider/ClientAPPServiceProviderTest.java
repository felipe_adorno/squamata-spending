package br.com.squamata.spending.service.provider;

import br.com.squamata.spending.service.model.v1.ClientAPPDomainModel;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public class ClientAPPServiceProviderTest extends TestSupport {

    @Mock
    private ClientAPPDomainModel clientAPPDomainModel;
    private ClientAPPServiceProvider clientAPPServiceProvider;

    @Before
    public void setUp() {
        clientAPPServiceProvider = new ClientAPPServiceProvider();
    }

    @Test
    public void testPersistUser() {
        when(clientAPPDomainModel.persist()).thenReturn(clientAPPDomainModel);
        clientAPPServiceProvider.persist(clientAPPDomainModel);
        verify(clientAPPDomainModel).persist();
    }

    @Test
    public void testFetchById() {
        when(clientAPPDomainModel.fetchById()).thenReturn(clientAPPDomainModel);
        clientAPPServiceProvider.fetchById(clientAPPDomainModel);
        verify(clientAPPDomainModel).fetchById();
    }
}
