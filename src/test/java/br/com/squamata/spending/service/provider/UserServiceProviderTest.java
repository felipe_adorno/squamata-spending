package br.com.squamata.spending.service.provider;

import br.com.squamata.spending.service.model.v1.UserDomainModel;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public class UserServiceProviderTest extends TestSupport {

    @Mock
    private UserDomainModel userDomainModel;
    private UserServiceProvider userServiceProvider;

    @Before
    public void setUp() {
        userServiceProvider = new UserServiceProvider();
    }

    @Test
    public void testPersistUser() {
        when(userDomainModel.persist()).thenReturn(userDomainModel);
        userServiceProvider.persist(userDomainModel);
        verify(userDomainModel).persist();
    }

    @Test
    public void testFetchByUserName() {
        when(userDomainModel.fetchByUserName()).thenReturn(userDomainModel);
        userServiceProvider.fetchByUserName(userDomainModel);
        verify(userDomainModel).fetchByUserName();
    }

    @Test
    public void testFetchById() {
        when(userDomainModel.fetchById()).thenReturn(userDomainModel);
        userServiceProvider.fetchById(userDomainModel);
        verify(userDomainModel).fetchById();
    }
}
