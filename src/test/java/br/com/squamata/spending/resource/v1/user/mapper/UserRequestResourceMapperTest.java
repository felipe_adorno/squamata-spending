package br.com.squamata.spending.resource.v1.user.mapper;

import br.com.squamata.spending.resource.v1.user.UserRequest;
import br.com.squamata.spending.service.model.v1.UserDomainModel;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public class UserRequestResourceMapperTest extends TestSupport {

    private UserDomainModel userDomainModel;
    private UserRequest userRequest;
    private UserRequestResourceMapper userRequestMapper;

    @Before
    public void setup() {
        userDomainModel = expectedUserDomainModel();
        userRequest = expectedUserRequest();
        userRequestMapper = new UserRequestResourceMapper();
    }

    @Test
    public void testMapRequest() {
        UserDomainModel mappedDomainModel = userRequestMapper.map(userRequest);
        assertEquals(userDomainModel, mappedDomainModel);
    }


    private UserRequest expectedUserRequest() {
        return new UserRequest("Felipe Adorno", "(11)96403-3391", "felipeadsc@gmail.com", "123456");
    }

    private UserDomainModel expectedUserDomainModel() {
        UserDomainModel userDomainModel = new UserDomainModel();
        userDomainModel.setPhone("(11)96403-3391");
        userDomainModel.setPassword("123456");
        userDomainModel.setUserName("felipeadsc@gmail.com");
        userDomainModel.setName("Felipe Adorno");
        userDomainModel.setAccountNonExpired(Boolean.TRUE);
        userDomainModel.setAccountNonLocked(Boolean.TRUE);
        userDomainModel.setCredentialsNonExpired(Boolean.TRUE);
        userDomainModel.setEnabled(Boolean.FALSE);
        List<String> roles = new ArrayList<String>();
        roles.add("ROLE_USER");
        userDomainModel.setRoles(roles);
        return userDomainModel;
    }

}
