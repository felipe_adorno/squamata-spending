package br.com.squamata.spending.resource.v1.user.mapper;

import br.com.squamata.spending.resource.v1.user.UserResponse;
import br.com.squamata.spending.service.model.UserModel;
import br.com.squamata.spending.service.model.v1.UserDomainModel;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public class UserResponseResourceMapperTest extends TestSupport {

    private String id;
    private UserResponseResourceMapper userResponseMapper;
    private UserModel userModel;
    private UserResponse userResponse;
    private Date insertDate;

    @Before
    public void setup() {
        insertDate = new Date();
        id = UUID.randomUUID().toString();
        userResponseMapper = new UserResponseResourceMapper();
        userModel = expectedUserDomainModel();
        userResponse = expectedUserResponse();
    }

    @Test
    public void testMapResponse() {
        UserResponse mappedResponse = userResponseMapper.map(userModel);
        assertEquals(userResponse, mappedResponse);
    }

    private UserResponse expectedUserResponse() {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(id);
        userResponse.setEnabled(Boolean.TRUE);
        userResponse.setName("Felipe Adorno");
        userResponse.setPhone("(11)96403-3391");
        userResponse.setUsername("felipeadsc@gmail.com");
        userResponse.setInsertDate(insertDate);
        return userResponse;
    }

    private UserDomainModel expectedUserDomainModel() {
        UserDomainModel userDomainModel = new UserDomainModel();
        userDomainModel.setId(id);
        userDomainModel.setPhone("(11)96403-3391");
        userDomainModel.setPassword("123456");
        userDomainModel.setUserName("felipeadsc@gmail.com");
        userDomainModel.setName("Felipe Adorno");
        userDomainModel.setAccountNonExpired(Boolean.TRUE);
        userDomainModel.setAccountNonLocked(Boolean.TRUE);
        userDomainModel.setCredentialsNonExpired(Boolean.TRUE);
        userDomainModel.setEnabled(Boolean.TRUE);
        List<String> roles = new ArrayList<String>();
        roles.add("ROLE_USER");
        userDomainModel.setRoles(roles);
        userDomainModel.setInsertDate(insertDate);
        return userDomainModel;
    }
}
