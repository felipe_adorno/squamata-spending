//package br.com.squamata.spending.resource.v1.user;
//
//import br.com.squamata.spending.config.Application;
//import br.com.squamata.spending.model.Role;
//import br.com.squamata.spending.spring.security.AuthenticationProvider;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.MockitoAnnotations;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.EnableAspectJAutoProxy;
//import org.springframework.http.MediaType;
//import org.springframework.mock.web.MockHttpSession;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.GrantedAuthorityImpl;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.oauth2.provider.ClientDetailsService;
//import org.springframework.security.web.FilterChainProxy;
//import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
///**
//* The Class UserResourceTest.
//*
//* @author cad_fadorno
//*/
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration
//@ContextConfiguration(classes = Application.class)
//public class UserResourceTest {
//
//    @Autowired
//    WebApplicationContext webApplicationContext;
//    @Autowired
//    private FilterChainProxy springSecurityFilterChain;
//    @InjectMocks
//    private UserResource userResource;
//    private MockMvc mockMvc;
//
//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
//                .addFilter(springSecurityFilterChain)
//                .build();
//    }
//
//    @Test
//    public void unauthorizedShowUser () throws Exception {
//        mockMvc.perform(get("/1/user")
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isUnauthorized());
//    }
//
//    @Test
//    public void unauthorizedCreateUser () throws Exception {
//        mockMvc.perform(post("/1/user")
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isUnauthorized());
//    }
//
//    public static class MockSecurityContext implements SecurityContext {
//
//        private static final long serialVersionUID = -1386535243513362694L;
//
//        private Authentication authentication;
//
//        public MockSecurityContext(Authentication authentication) {
//            this.authentication = authentication;
//        }
//
//        @Override
//        public Authentication getAuthentication() {
//            return this.authentication;
//        }
//
//        @Override
//        public void setAuthentication(Authentication authentication) {
//            this.authentication = authentication;
//        }
//    }
//
//    @Test
//    public void signedIn() throws Exception {
//
//        List<GrantedAuthority> auths = new ArrayList<>();
//        auths.add(new GrantedAuthorityImpl(Role.ROLE_USER.getRole()));
//
//        User user = new User("felipeadsc@gmail.com", "felipe", Boolean.TRUE,
//                Boolean.TRUE,
//                Boolean.TRUE,
//                Boolean.TRUE,
//                auths);
//
//        UsernamePasswordAuthenticationToken principal =  new UsernamePasswordAuthenticationToken(
//                user,
//                user.getPassword(),
//                user.getAuthorities());
//
//
//
//        MockHttpSession session = new MockHttpSession();
//        session.setAttribute(
//                HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
//                new MockSecurityContext(principal));
//
//
//        mockMvc
//                .perform(
//                        get("/1/user")
//                                .session(session))
//                .andExpect(status().isOk());
//    }
//}
