package br.com.squamata.spending.test.support;

import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@RunWith(MockitoJUnitRunner.class)
public abstract class TestSupport {

    @Before
    public void setUpTest() {
        MockitoAnnotations.initMocks(this);
    }

    public void assertEquals(Object expected, Object actual) {
        Assert.assertEquals(expected, actual);
    }

    public InOrder inOrder(Object... mocks) {
        return Mockito.inOrder(mocks);
    }

    public <T> OngoingStubbing<T> when(T methodCall) {
        return Mockito.when(methodCall);
    }

    public <T> T verify(T mock) {
        return Mockito.verify(mock);
    }
}
