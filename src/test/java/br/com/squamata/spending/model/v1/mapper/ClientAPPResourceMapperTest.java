package br.com.squamata.spending.model.v1.mapper;

import br.com.squamata.spending.model.AuthorizedGrantType;
import br.com.squamata.spending.model.ClientAPP;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.service.model.v1.ClientAPPDomainModel;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * The Class ClientAPPMapperTest.
 *
 * @author cad_fadorno
 */
public class ClientAPPResourceMapperTest extends TestSupport {

    private ClientAPP clientAPP;
    private ClientAPPDomainModel clientAPPDomainModel;
    private ClientAPPMapper clientAPPMapper;
    private String id;

    @Before
    public void setup() {
        id = UUID.randomUUID().toString();
        clientAPP = expectedClientAPP();
        clientAPPDomainModel = expectedClientAPPDomainModel();
        clientAPPMapper = new ClientAPPMapper();
    }

    @Test
    public void testMapDomainModel() {
        ClientAPPDomainModel mappedClientAPPDomainModel = clientAPPMapper.mapDomainModel(clientAPP);
        assertEquals(clientAPPDomainModel, mappedClientAPPDomainModel);
    }

    @Test
    public void testMapModel() {
        ClientAPP mappedClientAPP = clientAPPMapper.mapModel(clientAPPDomainModel);
        clientAPP.setId(mappedClientAPP.getId()); //generate by uuid

        assertEquals(clientAPP, mappedClientAPP);
    }

    private ClientAPP expectedClientAPP() {
        ClientAPP clientAPP = new ClientAPP();
        clientAPP.setId(id);
        clientAPP.setClientSecret("123");
        clientAPP.setAppName("squamata-app");

        List<Role> roles = new ArrayList<Role>();
        roles.add(Role.ROLE_USER);
        clientAPP.setRoles(roles);

        List<String> authorizedGrantTypes = new ArrayList<String>();
        authorizedGrantTypes.add(AuthorizedGrantType.PASSWORD.getAuthorizedGrantType());
        authorizedGrantTypes.add(AuthorizedGrantType.REFRESH_TOKEN.getAuthorizedGrantType());
        clientAPP.setAuthorizedGrantTypes(authorizedGrantTypes);

        return clientAPP;
    }

    private ClientAPPDomainModel expectedClientAPPDomainModel() {
        ClientAPPDomainModel clientAPPDomainModel = new ClientAPPDomainModel("squamata-app");
        clientAPPDomainModel.setClientSecret("123");
        clientAPPDomainModel.setId(id);

        List<String> authorizedGrantTypes = new ArrayList<String>();
        authorizedGrantTypes.add(AuthorizedGrantType.PASSWORD.getAuthorizedGrantType());
        authorizedGrantTypes.add(AuthorizedGrantType.REFRESH_TOKEN.getAuthorizedGrantType());
        clientAPPDomainModel.setAuthorizedGrantTypes(authorizedGrantTypes);

        List<String> roles = new ArrayList<String>();
        roles.add(Role.ROLE_USER.getRole());
        clientAPPDomainModel.setRoles(roles);

        return clientAPPDomainModel;
    }
}
