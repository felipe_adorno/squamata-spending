package br.com.squamata.spending.model.v1.mapper;

import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.model.User;
import br.com.squamata.spending.service.model.v1.UserDomainModel;
import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public class UserResourceMapperTest extends TestSupport {

    private User user;
    private UserDomainModel userDomainModel;
    private UserMapper userMapper;
    private String id;
    private Date insertDate;

    @Before
    public void setup() {
        insertDate = new Date();
        id = UUID.randomUUID().toString();
        user = expectedUser();
        userDomainModel = expectedUserDomainModel();
        userMapper = new UserMapper();
    }

    @Test
    public void testMapDomainModel() {
        UserDomainModel mappedDomainModel = userMapper.mapDomainModel(user);
        assertEquals(userDomainModel, mappedDomainModel);
    }

    @Test
    public void testMapModel() {
        User mappedUser = userMapper.mapModel(userDomainModel);

        user.setId(mappedUser.getId()); //generate by uuid
        user.setPassword(mappedUser.getPassword());//generate by crypt

        assertEquals(user, mappedUser);
    }

    private User expectedUser() {
        User user = new User();
        user.setId(id);
        user.setUserName("felipeadsc@gmail.com");
        user.setAccountNonExpired(Boolean.TRUE);
        user.setAccountNonLocked(Boolean.TRUE);
        user.setCredentialsNonExpired(Boolean.TRUE);
        user.setEnabled(Boolean.TRUE);
        user.setName("Felipe Adorno");
        user.setPassword("123456");
        user.setPhone("(11)96403-3391");
        List<Role> roles = new ArrayList<Role>();
        roles.add(Role.ROLE_USER);
        user.setRoles(roles);
        user.setInsertDate(insertDate);
        return user;
    }

    private UserDomainModel expectedUserDomainModel() {
        UserDomainModel userDomainModel = new UserDomainModel();
        userDomainModel.setId(id);
        userDomainModel.setPhone("(11)96403-3391");
        userDomainModel.setPassword("123456");
        userDomainModel.setUserName("felipeadsc@gmail.com");
        userDomainModel.setName("Felipe Adorno");
        userDomainModel.setAccountNonExpired(Boolean.TRUE);
        userDomainModel.setAccountNonLocked(Boolean.TRUE);
        userDomainModel.setCredentialsNonExpired(Boolean.TRUE);
        userDomainModel.setEnabled(Boolean.TRUE);
        List<String> roles = new ArrayList<String>();
        roles.add(Role.ROLE_USER.getRole());
        userDomainModel.setRoles(roles);
        userDomainModel.setInsertDate(insertDate);
        return userDomainModel;
    }
}
