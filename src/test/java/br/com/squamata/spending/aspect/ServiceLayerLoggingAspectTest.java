package br.com.squamata.spending.aspect;

import br.com.squamata.spending.exception.InternalArchitectureException;
import br.com.squamata.spending.test.support.TestSupport;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.fail;

/**
 * The Class ServiceLayerLoggingAspectTest.
 *
 * @author cad_fadorno
 */
public class ServiceLayerLoggingAspectTest extends TestSupport {

    @Mock
    private ProceedingJoinPoint joinPoint;
    @Mock
    private Signature signature;
    @InjectMocks
    private ServiceLayerLoggingAspect aspect = new ServiceLayerLoggingAspect();

    @Test
    public void testLogMethodSignatureParamsAndMetric() throws Throwable {
        Mockito.when(joinPoint.getThis()).thenReturn("This");
        String[] args = new String[2];
        args[0] = "arg0";
        args[1] = "arg1";
        Mockito.when(joinPoint.getArgs()).thenReturn(args);
        Mockito.when(joinPoint.getSignature()).thenReturn(signature);
        Mockito.when(signature.getName()).thenReturn("Signature");
        aspect.aroundServiceLayer(joinPoint);
    }

    @Test(expected = InternalArchitectureException.class)
    public void testeAroundException() throws Throwable {
        Mockito.when(joinPoint.getThis()).thenReturn("This");
        String[] args = new String[2];
        args[0] = "arg0";
        args[1] = "arg1";
        Mockito.when(joinPoint.getArgs()).thenReturn(args);
        Mockito.when(joinPoint.getSignature()).thenThrow(new InternalArchitectureException());
        try{
            aspect.aroundServiceLayer(joinPoint);
        }
        catch(InternalArchitectureException e){
            throw e;
        }
    }

    @Test(expected = InternalArchitectureException.class)
    public void testAroundExceptionLogExceptionAndExceptionInsideMethod() throws Throwable {
        Mockito.when(joinPoint.getThis()).thenReturn("This");
        String[] args = new String[2];
        args[0] = "arg0";
        args[1] = "arg1";
        Mockito.when(joinPoint.getArgs()).thenReturn(args);
        Mockito.when(joinPoint.getSignature()).thenThrow(new InternalArchitectureException());
        try{
            aspect.aroundServiceLayer(joinPoint);
            fail();
        }
        catch(InternalArchitectureException e){
            throw e;
        }
    }
}
