package br.com.squamata.spending.aspect;

import br.com.squamata.spending.test.support.TestSupport;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.mockito.Matchers.any;

/**
 * The Class RestErrorHandlerTest.
 *
 * @author cad_fadorno
 */
public class RestErrorHandlerTest extends TestSupport {
    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;
    @Mock
    private BindingResult bindingResult;
    @Mock
    private MessageSource messageSource;

    private HttpHeaders responseHeaders = createHttpHeaders();
    private List<FieldError> fieldErrors = createFieldErrors();
    @InjectMocks
    private RestErrorHandler restErrorHandler = new RestErrorHandler(messageSource);

    @Test
    public void testHandlerValidationException() {
        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
        when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);
        when(messageSource.getMessage(any(String.class), any(Object[].class), any(Locale.class))).thenReturn("The request is invalid or malformed.");
        when(messageSource.getMessage(any(FieldError.class), any(Locale.class))).thenReturn("error message");

        ResponseEntity<String> responseEntityToCompare = createResponseHandlerValidationException();
        ResponseEntity<String> responseEntity = restErrorHandler.handlerValidationException(methodArgumentNotValidException);
        assertEquals(responseEntity, responseEntityToCompare);
    }

    @Test
    public void testHandlerMissingServletRequestParameterException() {
        when(messageSource.getMessage(any(String.class), any(Object[].class), any(Locale.class))).thenReturn("The request is invalid or malformed.");
        ResponseEntity<String> responseEntityToCompare = createResponseHandlerMissingServletRequestParameterException();
        ResponseEntity<String> responseEntity = restErrorHandler.handlerMissingServletRequestParameterException(new Exception());
        assertEquals(responseEntity, responseEntityToCompare);
    }

    @Test
    public void testHandlerHttpMediaTypeNotSupportedException() {
        when(messageSource.getMessage(any(String.class), any(Object[].class), any(Locale.class))).thenReturn("The request entity is in a format not supported.");
        ResponseEntity<String> responseEntityToCompare = createResponseHandlerHttpMediaTypeNotSupportedException();
        ResponseEntity<String> responseEntity = restErrorHandler.handlerHttpMediaTypeNotSupportedException(new Exception());
        assertEquals(responseEntity, responseEntityToCompare);
    }

    private ResponseEntity<String> createResponseHandlerValidationException() {
        return new ResponseEntity<>(
                "{\"type\":\"Parameter_Error\",\"description\":\"The request is invalid or malformed.\"," +
                "\"date\":\""+new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date())+"\",\"notifications\":[\"error message\"]}",
                responseHeaders, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<String> createResponseHandlerMissingServletRequestParameterException() {
        return new ResponseEntity<>(
                "{\"type\":\"Parameter_Error\",\"description\":\"The request is invalid or malformed.\"," +
                        "\"date\":\""+new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date())+"\"}",
                responseHeaders, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<String> createResponseHandlerHttpMediaTypeNotSupportedException() {
        return new ResponseEntity<>(
                "{\"type\":\"Unsuported_MediaType_Error\",\"description\":\"The request entity is in a format not supported.\"," +
                        "\"date\":\""+new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date())+"\"}",
                responseHeaders, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    private HttpHeaders createHttpHeaders() {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=utf-8");
        return responseHeaders;
    }

    private List<FieldError> createFieldErrors() {
        List<FieldError> fieldErrors = new ArrayList<>();
        String[] codes = {"error message"};
        fieldErrors.add(new FieldError("field", "field", "error message", true, codes, null, "error message"));
        return fieldErrors;
    }
}
