package br.com.squamata.spending.annotation;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;

import java.lang.annotation.*;

/**
* A stereotype annotation to represent a class that belongs to the Model Layer.
* <p/>
* Domain Layer (or Model Layer): Responsible for representing concepts of the
* business, information about the business situation, and business rules. State
* that reflects the business situation is controlled and used here, even though
* the technical details of storing it are delegated to the infrastructure. This
* layer is the heart of business software.
* <p/>
* From P of EAA {@link http://martinfowler.com/eaaCatalog/domainModel.html}.
*
* @author Felipe Adorno (felipeadsc@gmail.com)
*/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ModelLayer {}
