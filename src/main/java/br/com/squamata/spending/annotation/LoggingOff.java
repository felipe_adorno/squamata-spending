package br.com.squamata.spending.annotation;

import java.lang.annotation.*;

/**
 * A markup annotation to turn off a the logging Aspect to a Method.
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoggingOff {
}
