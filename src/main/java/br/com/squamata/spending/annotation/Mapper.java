package br.com.squamata.spending.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * An object that sets up a communication between two independent objects.
 *
 * From P of EAA {@link http://martinfowler.com/eaaCatalog/mapper.html}.
 *
 * @author Felipe Adorno (felipeadsc@gmail.com)
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Mapper {

    /**
     * The value may indicate a suggestion for a logical component name,
     * to be turned into a Spring bean in case of an autodetected component.
     *
     * @return the suggested component name, if any
     */
    String value() default "";

}
