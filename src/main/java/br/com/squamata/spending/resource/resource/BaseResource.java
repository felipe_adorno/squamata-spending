package br.com.squamata.spending.resource.resource;

import br.com.squamata.spending.service.model.UserModel;
import br.com.squamata.spending.service.model.v1.UserDomainModel;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public abstract class BaseResource {

    public UserModel getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDomainModel userDomainModel = new UserDomainModel();
        userDomainModel.setUserName(auth.getName());
        return userDomainModel.fetchByUserName();
    }

}
