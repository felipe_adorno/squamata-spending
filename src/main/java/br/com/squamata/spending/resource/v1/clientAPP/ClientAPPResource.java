package br.com.squamata.spending.resource.v1.clientAPP;

import br.com.squamata.spending.resource.v1.clientAPP.mapper.ClientAPPRequestResourceMapper;
import br.com.squamata.spending.resource.v1.clientAPP.mapper.ClientAPPResponseResourceMapper;
import br.com.squamata.spending.service.ClientAPPService;
import br.com.squamata.spending.service.model.v1.ClientAPPDomainModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@RestController
@RequestMapping(value = "1/clientAPP")
@ExposesResourceFor(ClientAPPRequest.class)
public class ClientAPPResource {

    @Autowired
    private EntityLinks entityLinks;
    @Autowired
    private ClientAPPService clientAPPService;
    @Autowired
    private ClientAPPRequestResourceMapper clientAPPRequestMapper;
    @Autowired
    private ClientAPPResponseResourceMapper clientAPPResponseMapper;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public HttpEntity<Resource<ClientAPPResponse>> getClient(@PathVariable String id) {
        ClientAPPDomainModel clientAPPDomainModel = new ClientAPPDomainModel();
        clientAPPDomainModel.setId(id);

        Resource<ClientAPPResponse> resource =
                new Resource<ClientAPPResponse>(clientAPPResponseMapper.map(clientAPPService.fetchById(clientAPPDomainModel)));

        return new ResponseEntity<Resource<ClientAPPResponse>>(resource, HttpStatus.OK);
    }
//
//    @RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json")
//    public ResponseEntity<ClientAPPResponse> create(@Valid ClientAPPRequest clientAPPRequest) {
//        ClientAPPModel clientAPPModel = clientAPPService.persist(clientAPPRequestMapper.map(clientAPPRequest));
//        ClientAPPResponse clientAPPResponse = clientAPPResponseMapper.map(clientAPPModel);
//        return new ResponseEntity<ClientAPPResponse>(clientAPPResponse, HttpStatus.CREATED);
//    }
}
