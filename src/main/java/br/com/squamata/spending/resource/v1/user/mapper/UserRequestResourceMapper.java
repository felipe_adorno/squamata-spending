package br.com.squamata.spending.resource.v1.user.mapper;

import br.com.squamata.spending.annotation.Mapper;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.resource.mapper.ResourceMapper;
import br.com.squamata.spending.resource.v1.user.UserRequest;
import br.com.squamata.spending.service.model.v1.UserDomainModel;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Mapper
public class UserRequestResourceMapper implements ResourceMapper<UserDomainModel, UserRequest> {

    @Override
    public UserDomainModel map(UserRequest userRequest) {
        UserDomainModel userDomainModel =
                new UserDomainModel(userRequest.getName(), userRequest.getPhone(), userRequest.getUsername(),
                        userRequest.getPassword());
        userDomainModel.setEnabled(Boolean.FALSE);
        userDomainModel.setAccountNonExpired(Boolean.TRUE);
        userDomainModel.setCredentialsNonExpired(Boolean.TRUE);
        userDomainModel.setAccountNonLocked(Boolean.TRUE);
        userDomainModel.getRoles().add(Role.ROLE_USER.getRole());
        return userDomainModel;
    }
}
