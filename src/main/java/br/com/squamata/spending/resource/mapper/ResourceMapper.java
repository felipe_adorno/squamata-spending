package br.com.squamata.spending.resource.mapper;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public interface ResourceMapper<O, I> {
    O map(I i);
}
