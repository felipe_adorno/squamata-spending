package br.com.squamata.spending.resource.v1.clientAPP.mapper;

import br.com.squamata.spending.annotation.Mapper;
import br.com.squamata.spending.resource.mapper.ResourceMapper;
import br.com.squamata.spending.resource.v1.clientAPP.ClientAPPResponse;
import br.com.squamata.spending.service.model.ClientAPPModel;
import br.com.squamata.spending.service.model.v1.ClientAPPDomainModel;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Mapper
public class ClientAPPResponseResourceMapper implements ResourceMapper<ClientAPPResponse, ClientAPPModel> {
    @Override
    public ClientAPPResponse map(ClientAPPModel clientAPPModel) {
        ClientAPPDomainModel clientAPPDomainModel = (ClientAPPDomainModel) clientAPPModel;

        ClientAPPResponse clientAPPResponse = new ClientAPPResponse();
        clientAPPResponse.setAppName(clientAPPDomainModel.getAppName());
        clientAPPResponse.setRoles(clientAPPDomainModel.getRoles());
        clientAPPResponse.setAuthorizedGrantTypes(clientAPPDomainModel.getAuthorizedGrantTypes());
        clientAPPResponse.setClientSecret(clientAPPDomainModel.getClientSecret());
        clientAPPResponse.setId(clientAPPDomainModel.getId());

        return clientAPPResponse;
    }
}
