package br.com.squamata.spending.resource.v1.user;

import br.com.squamata.spending.resource.resource.BaseResource;
import br.com.squamata.spending.resource.v1.user.mapper.UserRequestResourceMapper;
import br.com.squamata.spending.resource.v1.user.mapper.UserResponseResourceMapper;
import br.com.squamata.spending.service.UserService;
import br.com.squamata.spending.service.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@RestController
@RequestMapping(value = "/1/user")
@ExposesResourceFor(UserRequest.class)
public class UserResource extends BaseResource {

    @Autowired
    private EntityLinks entityLinks;
    @Autowired
    private UserRequestResourceMapper userRequestMapper;
    @Autowired
    private UserResponseResourceMapper userResponseMapper;
    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody HttpEntity<Resource<UserResponse>> showUser() {
        Resource<UserResponse> userResponse = new Resource<UserResponse>(userResponseMapper.map(getLoggedUser()));
        userService.fetchById(getLoggedUser());
        userResponse.add(linkTo(methodOn(UserResource.class).showUser()).withSelfRel());
        return new ResponseEntity<Resource<UserResponse>>(userResponse, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody HttpEntity<Resource<UserResponse>> createUser(@Valid @RequestBody UserRequest userRequest) {
        UserModel userModel = userRequestMapper.map(userRequest);
        userModel.persist();
        Resource<UserResponse> userResponse = new Resource<UserResponse>(userResponseMapper.map(userModel));
        userResponse.add(linkTo(methodOn(UserResource.class).showUser()).withSelfRel());
        return new ResponseEntity<Resource<UserResponse>>(userResponse, HttpStatus.CREATED);
    }
}
