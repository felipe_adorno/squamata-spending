package br.com.squamata.spending.resource.v1.clientAPP.mapper;

import br.com.squamata.spending.annotation.Mapper;
import br.com.squamata.spending.model.AuthorizedGrantType;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.resource.mapper.ResourceMapper;
import br.com.squamata.spending.resource.v1.clientAPP.ClientAPPRequest;
import br.com.squamata.spending.service.model.v1.ClientAPPDomainModel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Mapper
public class ClientAPPRequestResourceMapper implements ResourceMapper<ClientAPPDomainModel, ClientAPPRequest> {
    @Override
    public ClientAPPDomainModel map(ClientAPPRequest clientAPPRequest) {
        ClientAPPDomainModel clientAPPDomainModel = new ClientAPPDomainModel(clientAPPRequest.getAppName());
        clientAPPDomainModel.setAuthorizedGrantTypes(mapDefaultAuthotizations(clientAPPDomainModel));
        clientAPPDomainModel.setRoles(mapDefaultRoles(clientAPPDomainModel));

        return clientAPPDomainModel;
    }

    private List<String> mapDefaultRoles(ClientAPPDomainModel clientAPPDomainModel) {
        List<String> roles = new ArrayList<String>();
        roles.add(Role.ROLE_USER.getRole());
        return roles;
    }

    private List<String> mapDefaultAuthotizations(ClientAPPDomainModel clientAPPDomainModel) {
        List<String> authorizations = new ArrayList<String>();
        authorizations.add(AuthorizedGrantType.PASSWORD.getAuthorizedGrantType());
        authorizations.add(AuthorizedGrantType.REFRESH_TOKEN.getAuthorizedGrantType());
        return authorizations;
    }
}
