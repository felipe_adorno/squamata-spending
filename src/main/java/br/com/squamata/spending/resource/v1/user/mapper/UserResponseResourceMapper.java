package br.com.squamata.spending.resource.v1.user.mapper;

import br.com.squamata.spending.annotation.Mapper;
import br.com.squamata.spending.resource.mapper.ResourceMapper;
import br.com.squamata.spending.resource.v1.user.UserResponse;
import br.com.squamata.spending.service.model.UserModel;
import br.com.squamata.spending.service.model.v1.UserDomainModel;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Mapper
public class UserResponseResourceMapper implements ResourceMapper<UserResponse, UserModel> {
    @Override
    public UserResponse map(UserModel userModel) {

        UserDomainModel userDomainModel = (UserDomainModel) userModel;

        UserResponse userResponse = new UserResponse();
        userResponse.setId(userDomainModel.getId());
        userResponse.setName(userDomainModel.getName());
        userResponse.setUsername(userDomainModel.getUserName());
        userResponse.setInsertDate(userDomainModel.getInsertDate());
        userResponse.setPhone(userDomainModel.getPhone());
        userResponse.setEnabled(userDomainModel.isEnabled());
        return userResponse;
    }
}
