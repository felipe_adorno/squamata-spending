package br.com.squamata.spending.util;

import org.springframework.util.Assert;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public final class ValidateUtils extends Assert {

    private ValidateUtils() {
    }

    private final static String invalidParam(final String paramName) {
        return String.format("%s is invalid.", paramName);
    }

    public static final void notNullParameter(Object param, final String paramName) {
        notNull(param, invalidParam(paramName));
    }
}
