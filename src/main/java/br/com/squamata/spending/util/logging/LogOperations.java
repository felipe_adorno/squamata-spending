package br.com.squamata.spending.util.logging;

import java.util.Map;


/**
 * Common Log Operations
 * <p/>.
 *
 * @author cad_ptoliveira
 * <p/>
 */
public interface LogOperations {

	/**
	 * Log a message as Info level.
	 * <p/>
	 *
	 * @param message the message
	 */
	void logInfo(final Object message);

	/**
	 * Log a message as Info level. With specified format.
	 * <p/>
	 *
	 * @param format   A <a href="../util/Formatter.html#syntax">format string</a>
	 * <p/>
	 * @param messages Objects to log for the specified format.
	 * <p/>
	 */
	void logInfo(final String format, final Object... messages);

	/**
	 * Log a message as Warn level.
	 * <p/>
	 *
	 * @param message the message
	 */
	void logWarn(final Object message);

	/**
	 * Log a message as Warn level. With specified format.
	 * <p/>
	 *
	 * @param format   A <a href="../util/Formatter.html#syntax">format string</a>
	 * <p/>
	 * @param messages Objects to log for the specified format.
	 * <p/>
	 */
	void logWarn(final String format, final Object... messages);

	/**
	 * Log a message as Error level.
	 * <p/>
	 *
	 * @param message the message
	 */
	void logError(final Object message);

	/**
	 * Log a message as Error level. With specified format.
	 * <p/>
	 *
	 * @param format   A <a href="../util/Formatter.html#syntax">format string</a>
	 * <p/>
	 * @param messages Objects to log for the specified format.
	 * <p/>
	 */
	void logError(final String format, final Object... messages);

	/**
	 * Log a message as Debug level.
	 * <p/>
	 *
	 * @param message the message
	 */
	void logDebug(final Object message);

	/**
	 * Log a message as Debug level. With specified format.
	 * <p/>
	 *
	 * @param format   A <a href="../util/Formatter.html#syntax">format string</a>
	 * <p/>
	 * @param messages Objects to log for the specified format.
	 * <p/>
	 */
	void logDebug(final String format, final Object... messages);

	/**
	 * Log a collection of key and value messages.
	 * <p/>
	 * @param logData <p/>
	 * @return a LogData
	 */
	LogData logData(final Map<String, Object> logData);

	/**
	 * Add a key to log
	 * <p/>.
	 *
	 * @param key key value. Can be formated with "=" or without it.
	 * <p/>
	 * @return SplunkLogKey
	 */
	LogKey logKey(final String key);
}
