package br.com.squamata.spending.util.logging;

import org.apache.log4j.Logger;


/**
 * LogKeyValue Builder.
 *
 * @author cad_ptoliveira
 */
public class LogKeyValueBuilder implements LogKey, RecursiveLogKey {

	/** The key. */
	private String key;
	
	/** The key value. */
	private final KeyValue keyValue;
		
	/**
	 * Instantiates a new log key value builder.
	 *
	 * @param logger the logger
	 * @param key the key
	 */
	private LogKeyValueBuilder(final Logger logger, final String key) {
		this.key = key;
		this.keyValue = new KeyValue(this, logger);
	}

	/**
	 * Creates the.
	 *
	 * @param logger the logger
	 * @param key the key
	 * @return the log key value builder
	 */
	protected static LogKeyValueBuilder create(final Logger logger, final String key) {
		return (key.contains("=")) ? new LogKeyValueBuilder(logger, key) : new LogKeyValueBuilder(logger, key + "=");
	}
	
	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogKey#value(java.lang.Object)
	 */
	@Override
	public RecursiveLogKey value(final Object msg) {
		return keyValue.setValue(key, msg);
	}
		
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	
	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.RecursiveLogKey#logKey(java.lang.String)
	 */
	@Override
	public LogKey logKey(String key) {
		return changeKey(key);
	}
	
	/**
	 * Change key.
	 *
	 * @param key the key
	 * @return the log key
	 */
	public LogKey changeKey(String key) {
		this.key = (key.contains("=")) ? key : key + "=";
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asInfo()
	 */
	@Override
	public AsLevel asInfo() {
		return keyValue.asInfo();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asWarn()
	 */
	@Override
	public AsLevel asWarn() {
		return keyValue.asWarn();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asError()
	 */
	@Override
	public AsLevel asError() {
		return keyValue.asError();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asDebug()
	 */
	@Override
	public AsLevel asDebug() {
		return keyValue.asDebug();
	}
}
