package br.com.squamata.spending.util.logging;


/**
 * LogKeyValueBuilderHolder holds multiples builders.
 * 
 * @author cad_ptoliveira
 *
 */
public class LogKeyValueBuilderHolder implements LogKey, RecursiveLogKey {
		
	/** The file log key value. */
	private final LogKeyValueBuilder fileLogKeyValue;
	
	/** The splunk log keyvalue. */
	private final LogKeyValueBuilder splunkLogKeyvalue;
	
	/**
	 * Construct a new LogKeyValueBuilderHolder.
	 *
	 * @param key the key
	 * @param fileLoggerDelegate the file logger delegate
	 * @param splunkLoggerDelegate the splunk logger delegate
	 */
	public LogKeyValueBuilderHolder(final String key, final FileLogger fileLoggerDelegate, final SplunkLogger splunkLoggerDelegate) {
		this.fileLogKeyValue = (LogKeyValueBuilder) fileLoggerDelegate.logKey(key);
		this.splunkLogKeyvalue = (LogKeyValueBuilder) splunkLoggerDelegate.logKey(key);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogKey#value(java.lang.Object)
	 */
	@Override
	public RecursiveLogKey value(Object msg) {
		fileLogKeyValue.value(msg);
		splunkLogKeyvalue.value(msg);
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asInfo()
	 */
	@Override
	public AsLevel asInfo() {
		fileLogKeyValue.asInfo();
		splunkLogKeyvalue.asInfo();
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asWarn()
	 */
	@Override
	public AsLevel asWarn() {
		fileLogKeyValue.asWarn();
		splunkLogKeyvalue.asWarn();
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asError()
	 */
	@Override
	public AsLevel asError() {
		fileLogKeyValue.asError();
		splunkLogKeyvalue.asError();
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asDebug()
	 */
	@Override
	public AsLevel asDebug() {
		fileLogKeyValue.asDebug();
		splunkLogKeyvalue.asDebug();
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.RecursiveLogKey#logKey(java.lang.String)
	 */
	@Override
	public LogKey logKey(String key) {
		fileLogKeyValue.logKey(key);
		splunkLogKeyvalue.logKey(key);
		return this;
	}
}
