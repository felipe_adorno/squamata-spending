package br.com.squamata.spending.util.logging;

import org.apache.log4j.Logger;

import java.util.Map;


/**
 * Splunk Logger Delegate
 * <p/>.
 *
 * @author cad_ptoliveira
 * <p/>
 */
public class SplunkLoggerDelegate implements SplunkLogger {

	/** The logger. */
	private final Logger logger;

	/**
	 * Instantiates a new splunk logger delegate.
	 *
	 * @param logger the logger
	 */
	private SplunkLoggerDelegate(final Logger logger) {
		this.logger = logger;
	}

	/**
	 * Creates the.
	 *
	 * @param logger the logger
	 * @return the splunk logger delegate
	 */
	protected static SplunkLoggerDelegate create(final Logger logger) {
		return new SplunkLoggerDelegate(logger);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logInfo(java.lang.Object)
	 */
	@Override
	public void logInfo(Object message) {
		logger.info(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logInfo(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logInfo(String format, Object... messages) {
		logger.info(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logWarn(java.lang.Object)
	 */
	@Override
	public void logWarn(Object message) {
		logger.warn(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logWarn(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logWarn(String format, Object... messages) {
		logger.warn(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logError(java.lang.Object)
	 */
	@Override
	public void logError(Object message) {
		logger.error(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logError(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logError(String format, Object... messages) {
		logger.error(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logDebug(java.lang.Object)
	 */
	@Override
	public void logDebug(Object message) {
		logger.debug(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logDebug(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logDebug(String format, Object... messages) {
		logger.debug(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logKey(java.lang.String)
	 */
	@Override
	public LogKey logKey(String key) {
		return LogKeyValueBuilder.create(logger, key);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logData(java.util.Map)
	 */
	@Override
	public LogData logData(final Map<String, Object> logData) {
		return new LogDataBuilder(logger, logData);
	}
}
