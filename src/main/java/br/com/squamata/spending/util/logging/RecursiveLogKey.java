package br.com.squamata.spending.util.logging;


/**
 * The Recursive LogKey Interface.
 *
 * @author cad_ptoliveira
 */
public interface RecursiveLogKey extends AsLevel {
	
	/**
	 * Add a key to log.
	 *
	 * @param key key value. Can be formated with "=" or without it.
	 * @return SplunkLogKey
	 */
	LogKey logKey(final String key);
}
