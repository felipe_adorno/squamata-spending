package br.com.squamata.spending.util.logging;

import org.apache.log4j.Logger;

import java.util.Map;


/**
 * Logger Holder
 * <p/>.
 *
 * @author cad_ptoliveira
 * <p/>
 */
public class LoggerHolder implements LogOperations {

	/** The file logger delegate. */
	private final FileLoggerDelegate fileLoggerDelegate;

	/** The splunk logger delegate. */
	private final SplunkLoggerDelegate splunkLoggerDelegate;

	/**
	 * Instantiates a new logger holder.
	 *
	 * @param clazz the clazz
	 */
	public LoggerHolder(final Class<?> clazz) {
		this.splunkLoggerDelegate = SplunkLoggerDelegate.create(Logger.getLogger("splunk=" + clazz.getName()));
		this.fileLoggerDelegate = FileLoggerDelegate.create(Logger.getLogger(clazz));
	}

	/**
	 * Gets the file logger delegate.
	 *
	 * @return the fileLoggerDelegate
	 */
	FileLoggerDelegate getFileLoggerDelegate() {
		return fileLoggerDelegate;
	}

	/**
	 * Gets the splunk logger delegate.
	 *
	 * @return the splunkLoggerDelegate
	 */
	SplunkLoggerDelegate getSplunkLoggerDelegate() {
		return splunkLoggerDelegate;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logInfo(java.lang.Object)
	 */
	@Override
	public void logInfo(final Object message) {
		fileLoggerDelegate.logInfo(message);
		splunkLoggerDelegate.logInfo(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logInfo(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logInfo(String format, Object... messages) {
		fileLoggerDelegate.logInfo(format, messages);
		splunkLoggerDelegate.logInfo(format, messages);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logWarn(java.lang.Object)
	 */
	@Override
	public void logWarn(final Object message) {
		fileLoggerDelegate.logWarn(message);
		splunkLoggerDelegate.logWarn(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logWarn(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logWarn(String format, Object... messages) {
		fileLoggerDelegate.logWarn(format, messages);
		splunkLoggerDelegate.logWarn(format, messages);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logError(java.lang.Object)
	 */
	@Override
	public void logError(final Object message) {
		fileLoggerDelegate.logError(message);
		splunkLoggerDelegate.logError(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logError(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logError(String format, Object... messages) {
		fileLoggerDelegate.logError(format, messages);
		splunkLoggerDelegate.logError(format, messages);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logDebug(java.lang.Object)
	 */
	@Override
	public void logDebug(final Object message) {
		fileLoggerDelegate.logDebug(message);
		splunkLoggerDelegate.logDebug(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logDebug(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logDebug(String format, Object... messages) {
		fileLoggerDelegate.logDebug(format, messages);
		splunkLoggerDelegate.logDebug(format, messages);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logData(java.util.Map)
	 */
	@Override
	public LogDataHolder logData(final Map<String, Object> logData) {
		return new LogDataHolder(logData, fileLoggerDelegate, splunkLoggerDelegate);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logKey(java.lang.String)
	 */
	@Override
	public LogKey logKey(String key) {
		return new LogKeyValueBuilderHolder(key, fileLoggerDelegate, splunkLoggerDelegate);
	}
}
