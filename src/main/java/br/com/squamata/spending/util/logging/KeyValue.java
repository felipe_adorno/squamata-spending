package br.com.squamata.spending.util.logging;

import org.apache.log4j.Logger;

import java.util.regex.Pattern;


/**
 * A Splunk log value wrapper.
 * 
 * @author cad_ptoliveira
 *
 */
public class KeyValue implements RecursiveLogKey {
	
	/** The value. */
	private final StringBuilder value;
	
	/** The builder. */
	private final LogKeyValueBuilder builder;
	
	/** The logger. */
	private final Logger logger;	
	
	/** The pattern. */
	private final Pattern pattern = Pattern.compile("\\s");
			
	/**
	 * Instantiates a new key value.
	 *
	 * @param builder the builder
	 * @param logger the logger
	 */
	public KeyValue(LogKeyValueBuilder builder, Logger logger) {
		super();
		this.builder = builder;
		this.logger = logger;
		this.value = new StringBuilder();
	}
	
	/**
	 * Gets the logger.
	 *
	 * @return the logger
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Gets the value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value.toString();
	}

	/**
	 * Set a key and a value.
	 *
	 * @param key the key
	 * @param value the value to set
	 * @return a RecursiveLogKey
	 */
	public RecursiveLogKey setValue(String key, Object value) {
		this.value.append(key);		
		if (stringHasSpaces(String.valueOf(value))) {
			this.value.append("\"").append(value).append("\" ");			
		} else {
			this.value.append(value).append(" ");
		}
		return this;
	}
	
	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.RecursiveLogKey#logKey(java.lang.String)
	 */
	@Override
	public LogKey logKey(String key) {
		return builder.changeKey(key);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asInfo()
	 */
	@Override
	public AsLevel asInfo() {
		logger.info(value.toString());
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asWarn()
	 */
	@Override
	public AsLevel asWarn() {
		logger.warn(value.toString());
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asError()
	 */
	@Override
	public AsLevel asError() {
		logger.error(value.toString());
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asDebug()
	 */
	@Override
	public AsLevel asDebug() {
		logger.debug(value.toString());
		return this;
	}
	
	/**
	 * String has spaces.
	 *
	 * @param s the s
	 * @return true, if successful
	 */
	private boolean stringHasSpaces(final String s) {
		return pattern.matcher(s).find();
	}
}
