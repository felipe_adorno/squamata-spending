package br.com.squamata.spending.util.logging;


/**
 * Fluent logger is a interface that encapsulates common log operations to be used in a fluent way.
 * 
 * @author cad_ptoliveira
 * @see LoggerService
 */
public interface FluentLogger {
	
	/**
	 * Invoke Operations in all loggers.
	 *
	 * @return the log operations
	 */
	LogOperations all();
	
	/**
	 * Invoke File Logger Operations.
	 *
	 * @return the file logger
	 */
	FileLogger file();
	
	/**
	 * Invoke Splunk Logger Operations.
	 *
	 * @return the splunk logger
	 */
	SplunkLogger splunk();
}
