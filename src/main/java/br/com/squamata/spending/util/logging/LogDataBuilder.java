package br.com.squamata.spending.util.logging;

import org.apache.log4j.Logger;

import java.util.Map;
import java.util.regex.Pattern;


/**
 * The Log Data Builder.
 *
 * @author cad_ptoliveira
 */
public class LogDataBuilder implements LogData {

	/** The logger. */
	private final Logger logger;
	
	/** The data. */
	private final String data;
	
	/** The t. */
	private final Throwable t;

	/**
	 * Instantiates a new log data builder.
	 *
	 * @param logger the logger
	 * @param logData the log data
	 */
	public LogDataBuilder(final Logger logger, final Map<String, Object> logData) {
		this(logger, logData, null);
	}

	/**
	 * Instantiates a new log data builder.
	 *
	 * @param logger the logger
	 * @param logData the log data
	 * @param t the t
	 */
	public LogDataBuilder(final Logger logger, final Map<String, Object> logData, final Throwable t) {
		this.logger = logger;
		this.data = transformLogData(logData);
		this.t = t;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asInfo()
	 */
	@Override
	public AsLevel asInfo() {
		if (t == null) {
			logger.info(data);
		} else {
			logger.info(data, t);
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asWarn()
	 */
	@Override
	public AsLevel asWarn() {
		if (t == null) {
			logger.warn(data);
		} else {
			logger.warn(data, t);
		}		
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asError()
	 */
	@Override
	public AsLevel asError() {
		if (t == null) {
			logger.error(data);
		} else {
			logger.error(data, t);
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asDebug()
	 */
	@Override
	public AsLevel asDebug() {
		if (t == null) {
			logger.debug(data);
		} else {
			logger.debug(data, t);
		}
		return this;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogData#getData()
	 */
	@Override
	public String getData() {
		return data;
	}

	/**
	 * Transform log data.
	 *
	 * @param logData the log data
	 * @return the string
	 */
	private String transformLogData(final Map<String, Object> logData) {
		StringBuilder sb = new StringBuilder(100);
		for (String key : logData.keySet()) {
			sb.append(key);
			final String value = String.valueOf(logData.get(key));
			if (stringHasSpaces(value)) {
				sb.append("=\"").append(value).append("\" ");
			} else {
				sb.append("=").append(value).append(" ");
			}
		}
		return sb.toString();
	}

	/**
	 * String has spaces.
	 *
	 * @param s the s
	 * @return true, if successful
	 */
	private boolean stringHasSpaces(final String s) {
		return Pattern.compile("\\s").matcher(s).find();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((logger == null) ? 0 : logger.hashCode());
		result = prime * result + ((t == null) ? 0 : t.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof LogDataBuilder)) {
			return false;
		}
		LogDataBuilder other = (LogDataBuilder) obj;
		if (data == null) {
			if (other.data != null) {
				return false;
			}
		} else if (!data.equals(other.data)) {
			return false;
		}
		if (logger == null) {
			if (other.logger != null) {
				return false;
			}
		} else if (!logger.equals(other.logger)) {
			return false;
		}
		if (t == null) {
			if (other.t != null) {
				return false;
			}
		} else if (!t.equals(other.t)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("LogDataBuilder [logger=%s, data=%s, t=%s]", logger, data, t);
	}
}
