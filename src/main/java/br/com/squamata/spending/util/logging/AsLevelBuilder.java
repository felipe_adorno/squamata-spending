package br.com.squamata.spending.util.logging;

import java.util.Map;


/**
 * Holder of AsLevel.
 *
 * @author cad_ptoliveira
 */
public class AsLevelBuilder implements AsLevel {

	/** The log data. */
	private final Map<String, Object> logData;	
	
	/** The file logger delegate. */
	private final FileLogger fileLoggerDelegate;
	
	/** The splunk logger delegate. */
	private final SplunkLogger splunkLoggerDelegate;

	/**
	 * Instantiates a new as level builder.
	 *
	 * @param logData the log data
	 * @param fileLoggerDelegate the file logger delegate
	 * @param splunkLoggerDelegate the splunk logger delegate
	 */
	public AsLevelBuilder(Map<String, Object> logData, FileLogger fileLoggerDelegate,
			SplunkLogger splunkLoggerDelegate) {
		super();
		this.logData = logData;
		this.fileLoggerDelegate = fileLoggerDelegate;
		this.splunkLoggerDelegate = splunkLoggerDelegate;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.uol.egw.util.logging.AsLevel#asInfo()
	 */
	@Override
	public AsLevel asInfo() {
		fileLoggerDelegate.logData(logData).asInfo();
		splunkLoggerDelegate.logData(logData).asInfo();
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.uol.egw.util.logging.AsLevel#asWarn()
	 */
	@Override
	public AsLevel asWarn() {
		fileLoggerDelegate.logData(logData).asWarn();
		splunkLoggerDelegate.logData(logData).asWarn();
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.uol.egw.util.logging.AsLevel#asError()
	 */
	@Override
	public AsLevel asError() {
		fileLoggerDelegate.logData(logData).asError();
		splunkLoggerDelegate.logData(logData).asError();
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.uol.egw.util.logging.AsLevel#asDebug()
	 */
	@Override
	public AsLevel asDebug() {
		fileLoggerDelegate.logData(logData).asDebug();
		splunkLoggerDelegate.logData(logData).asDebug();
		return this;
	}
}
