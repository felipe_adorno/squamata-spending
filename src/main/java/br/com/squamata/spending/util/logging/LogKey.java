package br.com.squamata.spending.util.logging;


/**
 * Spkunk Log Key.
 *
 * @author cad_ptoliveira
 */
public interface LogKey {
	
	/**
	 * Attach a value to this correspondent key.
	 * 
	 * @param msg to attach
	 * @return a SplunkAsLevel
	 */
	RecursiveLogKey value(Object msg);
}
