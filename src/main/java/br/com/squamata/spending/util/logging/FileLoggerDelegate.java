package br.com.squamata.spending.util.logging;

import org.apache.log4j.Logger;

import java.util.Map;


/**
 * File Logger Delegate
 * <p/>.
 *
 * @author cad_ptoliveira
 * <p/>
 */
public class FileLoggerDelegate implements FileLogger {

	/** The logger. */
	private final Logger logger;

	/**
	 * Instantiates a new file logger delegate.
	 *
	 * @param logger the logger
	 */
	private FileLoggerDelegate(final Logger logger) {
		this.logger = logger;
	}

	/**
	 * Creates the.
	 *
	 * @param logger the logger
	 * @return the file logger delegate
	 */
	protected static FileLoggerDelegate create(final Logger logger) {
		return new FileLoggerDelegate(logger);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logInfo(java.lang.Object)
	 */
	@Override
	public void logInfo(Object message) {
		logger.info(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logInfo(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logInfo(String format, Object... messages) {
		logger.info(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logWarn(java.lang.Object)
	 */
	@Override
	public void logWarn(Object message) {
		logger.warn(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logWarn(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logWarn(String format, Object... messages) {
		logger.warn(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logError(java.lang.Object)
	 */
	@Override
	public void logError(Object message) {
		logger.error(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logError(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logError(String format, Object... messages) {
		logger.error(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logDebug(java.lang.Object)
	 */
	@Override
	public void logDebug(Object message) {
		logger.debug(message);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logDebug(java.lang.String, java.lang.Object[])
	 */
	@Override
	public void logDebug(String format, Object... messages) {
		logger.debug(String.format(format, messages));
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FileLogger#logInfo(java.lang.Object, java.lang.Throwable)
	 */
	@Override
	public void logInfo(Object message, Throwable t) {
		logger.info(message, t);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FileLogger#logWarn(java.lang.Object, java.lang.Throwable)
	 */
	@Override
	public void logWarn(Object message, Throwable t) {
		logger.warn(message, t);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FileLogger#logError(java.lang.Object, java.lang.Throwable)
	 */
	@Override
	public void logError(Object message, Throwable t) {
		logger.error(message, t);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FileLogger#logDebug(java.lang.Object, java.lang.Throwable)
	 */
	@Override
	public void logDebug(Object message, Throwable t) {
		logger.debug(message, t);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logData(java.util.Map)
	 */
	@Override
	public LogData logData(Map<String, Object> logData) {
		return new LogDataBuilder(logger, logData);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FileLogger#logData(java.util.Map, java.lang.Throwable)
	 */
	@Override
	public LogData logData(Map<String, Object> logData, Throwable t) {
		return new LogDataBuilder(logger, logData, t);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogOperations#logKey(java.lang.String)
	 */
	@Override
	public LogKey logKey(String key) {
		return LogKeyValueBuilder.create(logger, key);
	}
}
