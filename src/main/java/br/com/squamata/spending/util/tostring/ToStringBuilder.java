package br.com.squamata.spending.util.tostring;

import br.com.squamata.spending.util.ValidateUtils;
import br.com.squamata.spending.util.logging.FluentLogger;
import br.com.squamata.spending.util.logging.LoggerService;
import br.com.squamata.spending.util.tostring.annotation.ToStringExclude;
import br.com.squamata.spending.util.tostring.annotation.ToStringStyle;
import br.com.squamata.spending.util.tostring.annotation.ToStringStyle.Style;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * ToStringBuilder.
 * <p>
 * @author cad_ptoliveira
 */
public final class ToStringBuilder {

	private static final FluentLogger logger = LoggerService.init(ToStringBuilder.class);
	private static final int PAGE_SIZE = 15;
	private static final String EQUAL = "=";
	private static final String COMMA = ", ";
	private static final boolean IGNORE_SUPER_TYPES = true;
	private static final boolean NOT_IGNORE_SUPER_TYPES = false;
	private static final String DEFAULT_ERROR = "<We got some error on ToStringBuilder!>";
	private static final String TO_MANY_TO_STRING_CALLS_VIOLATION = "<We got some error on ToStringBuilder, reflectionToString"
			+ " method has reached the max depth calls! Please verify the circular references.>";

	public static String reflectionToString(final Object o) {
		return reflectionToString(o, NOT_IGNORE_SUPER_TYPES);
	}

	public static String reflectionToString(final Object o, boolean ignoreSuperType) {
		return reflectionToString(o, ignoreSuperType, null);
	}

	private static String reflectionToString(Object o, boolean ignoreSuperType, Style style) {
		if (o == null) {
			return "Object=null ";
		}
		StringBuilder builder = new StringBuilder();
		if (RecursiveToStringDepthController.isAllowed()) {
			if (isArray(o)) {
				appendArrayValues(o, builder);
			} else if (isWrapper(o)) {
				builder.append("[");
				builder.append(o);
				builder.append("]");
			} else if (isCollection(o)) {
				appendCollection((Collection<?>) o, builder);
			} else if (isMap(o)) {
				appendMap((Map<?, ?>) o, builder);
			} else {
				builder.append(o.getClass().getSimpleName());
				builder.append("[");
				builder.append(toStringFields(o, ignoreSuperType, style));
				builder.delete(builder.length() - 2, builder.length());
				builder.append("]");
			}
		} else {
			builder.append("[");
			builder.append(TO_MANY_TO_STRING_CALLS_VIOLATION);
			builder.append("]");
		}
		return builder.toString();
	}

	private static String toStringFields(Object o, boolean ignoreSuperType, Style style) {
		StringBuilder builder = new StringBuilder();
		Field[] fields = (ignoreSuperType) ? o.getClass().getDeclaredFields() : extractSuperClassFields(o);
		for (Field field : fields) {
			String name = field.getName();
			if (isToPrintField(field, name)) {
				Style selectedStyle = selectStyle(style, field);
				appendValue(o, field, name, builder, selectedStyle);
			}
		}
		return builder.toString();
	}

	private static Style selectStyle(Style current, Field field) {
		return (current == null) ? getFieldStyle(field) : current;
	}

	private static Style getFieldStyle(Field field) {
		return (field.isAnnotationPresent(ToStringStyle.class))
				? field.getAnnotation(ToStringStyle.class).value()
				: Style.REFLECTION;
	}

	private static Field[] extractSuperClassFields(Object o) {
		Field[] classFields = o.getClass().getDeclaredFields();
		if (hasSuperType(o)) {
			Field[] superFields = o.getClass().getSuperclass().getDeclaredFields();
			List<Field> fields = new ArrayList<>();
			for (Field field : superFields) {
				if (isNotOn(classFields, field)) {
					fields.add(field);
				}
			}
			if (fields.isEmpty()) {
				return classFields;
			} else {
				Collections.addAll(fields, classFields);
				return fields.toArray(new Field[]{});
			}
		} else {
			return classFields;
		}
	}

	private static boolean isNotOn(Field[] classFields, Field field) {
		for (Field classField : classFields) {
			if (field.getName().equals(classField.getName()) && field.getType().equals(classField.getType())) {
				return false;
			}
		}
		return true;
	}

	private static boolean hasSuperType(Object o) {
		return !(o.getClass().getSuperclass().equals(Object.class));
	}

	private static boolean isToPrintField(Field field, String name) {
		return !(field.isAnnotationPresent(ToStringExclude.class) || "serialVersionUID".equals(name) || "this$0".equals(name) || Modifier
				.isStatic(field.getModifiers()));
	}

	private static void appendValue(Object o, final Field field, final String name, final StringBuilder builder, Style style) {
		try {
			if (!field.isAccessible()) {
				field.setAccessible(true);
			}
			Object value = field.get(o);
			if (value == null) {
				if (Style.IGNORE_NULL.equals(style)) {
					return;
				}
				builder.append(name).append(EQUAL);
				builder.append("null");
			} else if (isArray(value)) {
				builder.append(name).append(EQUAL);
				appendArrayValues(o, field, builder);
			} else if (isWrapper(value) || Style.CALL_TO_STRING.equals(style) || Style.MASK_FIELD.equals(style) || haveImplementedToString(value)) {
				builder.append(name).append(EQUAL);
				builder.append(Style.MASK_FIELD.equals(style) ? maskField(value) : value.toString());
			} else if (isCollection(value)) {
				builder.append(name).append(EQUAL);
				appendCollection((Collection<?>) value, builder);
			} else if (isMap(value)) {
				builder.append(name).append(EQUAL);
				appendMap((Map<?, ?>) value, builder);
			} else {
				builder.append(name).append(EQUAL);
				builder.append(reflectionToString(value, IGNORE_SUPER_TYPES, style));
			}

			builder.append(COMMA);

		} catch (Exception ex) {
			handleException(ex, builder);
		}
	}

	private static void handleException(Exception ex, StringBuilder builder) {
		logger.file().logError(DEFAULT_ERROR, ex);
		builder.append(DEFAULT_ERROR);
	}

	public static String mask(String value) {
		ValidateUtils.notNullParameter(value, "value");
		return maskField(value).toString();
	}

	private static Object maskField(Object value) {
		String strValue = value.toString();
		final int size = strValue.length();

		if (size < 11) {
			return strValue.replaceAll("\\.", "*");
		}

		final int lastPart = 4;
		final int firstPart = 6;
		final int maskSize = size - lastPart - firstPart;

		final StringBuilder sb = new StringBuilder(strValue.substring(0, firstPart));
		for (int i = 0; i < maskSize; i++) {
			sb.append("*");
		}
		sb.append(strValue.substring(size - lastPart, size));
		return sb.toString();
	}

	private static boolean isArray(Object value) {
		return value.getClass().isArray();
	}

	private static boolean haveImplementedToString(Object value) {
		try {
			Method toString = value.getClass().getDeclaredMethod("toString");
			return (toString != null);
		} catch (SecurityException | NoSuchMethodException e) {
			return false;
		}
	}

	private static boolean isWrapper(Object value) {
		return (value.getClass().isPrimitive()
				|| value.getClass().isInterface()
				|| value instanceof String
				|| value instanceof Integer
				|| value instanceof Double
				|| value instanceof Short
				|| value instanceof Long
				|| value instanceof Float
				|| value instanceof Character
				|| value instanceof Boolean
				|| value instanceof Byte
				|| value instanceof Date
				|| value instanceof Calendar
				|| value instanceof Locale
				|| value instanceof Date
				|| value instanceof Class<?>
				|| value instanceof Enum<?>);
	}

	private static boolean isMap(Object value) {
		return (value instanceof Map);
	}

	private static boolean isCollection(Object value) {
		return (value instanceof Collection);
	}

	private static void appendArrayValues(Object o, Field field, StringBuilder builder) throws IllegalArgumentException,
			IllegalAccessException {
		Object array = field.get(o);
		appendArrayValues(array, builder);
	}

	private static void appendArrayValues(Object array, StringBuilder builder) {
		int lenght = Array.getLength(array);
		builder.append("[");
		if (lenght > PAGE_SIZE) {
			appendArrayValuesRange(array, builder, 0, PAGE_SIZE);
			builder.append(", ...");
		} else {
			appendArrayValuesRange(array, builder, 0, lenght);
		}
		builder.append("]");
	}

	private static void appendArrayValuesRange(Object array, StringBuilder builder, int beginIndex, int endIndex) {
		for (int i = beginIndex; i < endIndex; i++) {
			Object element = Array.get(array, i);
			if (element != null && element.getClass().isArray()) {
				appendArrayValues(element, builder);
			} else {
				builder.append(element);
				if (i != (endIndex - 1)) {
					builder.append(COMMA);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private static void appendCollection(Collection<?> collection, StringBuilder builder) {
		try {
			builder.append(collection.getClass().getSimpleName()).append("{ ");
			int counter = 0;
			for (Iterator<Object> it = (Iterator<Object>) collection.iterator(); it.hasNext();) {
				Object element = it.next();
				appendCollectionValue(element, builder);
				if (it.hasNext()) {
					builder.append(COMMA);
				}
				counter++;
				if (counter == PAGE_SIZE) {
					builder.append(", ...");
					break;
				}
			}
			builder.append(" }");
		} catch (Exception ex) {
			handleException(ex, builder);
		}
	}

	private static void appendCollectionValue(Object element, StringBuilder builder) {
		try {
			if (element == null) {
				builder.append(element);
			} else if (isArray(element)) {
				appendArrayValues(element, builder);
			} else if (isWrapper(element) || haveImplementedToString(element)) {
				builder.append("[");
				builder.append(element);
				builder.append("]");
			} else if (isCollection(element)) {
				appendCollection((Collection<?>) element, builder);
			} else if (isMap(element)) {
				appendMap((Map<?, ?>) element, builder);
			} else {
				builder.append(reflectionToString(element, IGNORE_SUPER_TYPES));
			}
		} catch (Exception ex) {
			handleException(ex, builder);
		}
	}

	@SuppressWarnings("unchecked")
	private static void appendMap(Map<?, ?> map, StringBuilder builder) {
		try {
			builder.append("Map{ ");
			int counter = 1;
			for (Iterator<Object> keyIt = (Iterator<Object>) map.keySet().iterator(); keyIt.hasNext();) {
				Object key = keyIt.next();
				builder.append("(k").append(counter).append(EQUAL);
				builder.append(key);
				builder.append(", v").append(counter).append(EQUAL);
				appendCollectionValue(map.get(key), builder);
				builder.append(")");
				if (keyIt.hasNext()) {
					builder.append(COMMA);
				}
				counter++;
			}
			builder.append(" }");
		} catch (Exception ex) {
			handleException(ex, builder);
		}
	}
}
