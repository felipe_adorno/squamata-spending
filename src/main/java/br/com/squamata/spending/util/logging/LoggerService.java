package br.com.squamata.spending.util.logging;


import static br.com.squamata.spending.util.ValidateUtils.notNullParameter;

/**
 * Implements FluentLogger.
 *
 * @author cad_ptoliveira
 */
public class LoggerService implements FluentLogger {

	/** The holder. */
	private final LoggerHolder holder;

	/**
	 * Instantiates a new logger service.
	 *
	 * @param clazz the clazz
	 */
	private LoggerService(Class<?> clazz) {
		notNullParameter(clazz, "Class");
		holder = new LoggerHolder(clazz);
	}
	
	/**
	 * Initialize a new Fluent Logger based on a class.
	 *
	 * @param clazz the clazz
	 * @return FluentLogger
	 */
	public static FluentLogger init(final Class<?> clazz) {
		return new LoggerService(clazz);
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FluentLogger#all()
	 */
	@Override
	public LogOperations all() {
		return holder;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FluentLogger#file()
	 */
	@Override
	public FileLogger file() {
		return holder.getFileLoggerDelegate();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.FluentLogger#splunk()
	 */
	@Override
	public SplunkLogger splunk() {
		return holder.getSplunkLoggerDelegate();
	}
}
