package br.com.squamata.spending.util.tostring.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Call the toString method from the field.
 * <p>
 * @author cad_ptoliveira
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ToStringStyle {

	/**
	 * The Enum Style.
	 */
	public enum Style {
		REFLECTION,
		IGNORE_NULL,
		CALL_TO_STRING,
		MASK_FIELD
	}

	/**
	 * Value.
	 *
	 * @return the style
	 */
	public Style value() default Style.REFLECTION;
}
