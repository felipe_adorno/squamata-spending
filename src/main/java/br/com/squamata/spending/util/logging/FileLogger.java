package br.com.squamata.spending.util.logging;

import java.util.Map;


/**
 * The Simple File Logger Interface.
 * 
 * @author cad_ptoliveira
 * 
 */
public interface FileLogger extends LogOperations {
	
	/**
	 * Log info.
	 *
	 * @param message the message
	 * @param t the t
	 */
	void logInfo(final Object message, final Throwable t);

	/**
	 * Log warn.
	 *
	 * @param message the message
	 * @param t the t
	 */
	void logWarn(final Object message, final Throwable t);

	/**
	 * Log error.
	 *
	 * @param message the message
	 * @param t the t
	 */
	void logError(final Object message, final Throwable t);

	/**
	 * Log debug.
	 *
	 * @param message the message
	 * @param t the t
	 */
	void logDebug(final Object message, final Throwable t);
	
	/**
	 * Log data.
	 *
	 * @param logData the log data
	 * @param t the t
	 * @return the log data
	 */
	LogData logData(final Map<String, Object> logData, final Throwable t);
}
