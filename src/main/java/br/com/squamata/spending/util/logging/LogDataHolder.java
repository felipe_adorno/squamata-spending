package br.com.squamata.spending.util.logging;

import java.util.Map;


/**
 * The Class LogDataHolder.
 *
 * @author cad_ptoliveira
 */
public class LogDataHolder implements LogData {
	
	/** The log data. */
	private final Map<String, Object> logData;
	
	/** The file logger delegate. */
	private final FileLogger fileLoggerDelegate;	
	
	/** The splunk logger delegate. */
	private final SplunkLogger splunkLoggerDelegate;
	
	/**
	 * Instantiates a new log data holder.
	 *
	 * @param logData the log data
	 * @param fileLoggerDelegate the file logger delegate
	 * @param splunkLoggerDelegate the splunk logger delegate
	 */
	public LogDataHolder(final Map<String, Object> logData, FileLogger fileLoggerDelegate, SplunkLogger splunkLoggerDelegate) {
		super();
		this.logData = logData;
		this.fileLoggerDelegate = fileLoggerDelegate;
		this.splunkLoggerDelegate = splunkLoggerDelegate;
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asInfo()
	 */
	@Override
	public AsLevel asInfo() {
		return new AsLevelBuilder(logData, fileLoggerDelegate, splunkLoggerDelegate).asInfo();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asWarn()
	 */
	@Override
	public AsLevel asWarn() {
		return new AsLevelBuilder(logData, fileLoggerDelegate, splunkLoggerDelegate).asWarn();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asError()
	 */
	@Override
	public AsLevel asError() {
		return new AsLevelBuilder(logData, fileLoggerDelegate, splunkLoggerDelegate).asError();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.AsLevel#asDebug()
	 */
	@Override
	public AsLevel asDebug() {
		return new AsLevelBuilder(logData, fileLoggerDelegate, splunkLoggerDelegate).asDebug();
	}

	/* (non-Javadoc)
	 * @see br.com.uol.egw.util.logging.LogData#getData()
	 */
	@Override
	public String getData() {
		return fileLoggerDelegate.logData(logData).getData();
	}
}
