package br.com.squamata.spending.util.logging;


/**
 * Represents a bunch of log data.
 * @author cad_ptoliveira
 */
public interface LogData extends AsLevel {
	
	/**
	 * Retrieve holded data as String.
	 *
	 * @return the data
	 */
	String getData();
}
