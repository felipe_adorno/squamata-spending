package br.com.squamata.spending.util.logging;


/**
 * Interface that contains methods "as some log level" to use in a fluent way.
 * @author cad_ptoliveira
 *
 */
public interface AsLevel {
	
	/**
	 * Log information as Info.
	 *
	 * @return AsLevel
	 */
	AsLevel asInfo();
	
	/**
	 * Log information as Warn.
	 *
	 * @return AsLevel
	 */
	AsLevel asWarn();
	
	/**
	 * Log information as Error.
	 *
	 * @return AsLevel
	 */
	AsLevel asError();
	
	/**
	 * Log information as Debug.
	 *
	 * @return AsLevel
	 */
	AsLevel asDebug();
}
