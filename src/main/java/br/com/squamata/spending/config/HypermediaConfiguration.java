package br.com.squamata.spending.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.hateoas.config.EnableHypermediaSupport;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Configuration
@Profile("hateoas")
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class HypermediaConfiguration {

}