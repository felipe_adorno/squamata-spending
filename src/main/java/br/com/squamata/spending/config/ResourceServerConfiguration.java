package br.com.squamata.spending.config;

import br.com.squamata.spending.spring.security.AuthenticationProvider;
import br.com.squamata.spending.spring.security.entrypoint.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Value("${resourceId}")
    private String resourceId;

    @Autowired
    private ResourceServerTokenServices squamataTokenService;
    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.tokenServices(squamataTokenService).resourceId(resourceId);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authenticationProvider(authenticationProvider)
                .authorizeRequests().antMatchers(HttpMethod.POST, "/1/user**").hasAuthority("ROLE_API")
                .and().authorizeRequests().antMatchers("/1/**").hasRole("USER")
                .and().httpBasic().authenticationEntryPoint(restAuthenticationEntryPoint);
    }
}
