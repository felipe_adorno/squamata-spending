package br.com.squamata.spending.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Order(0)
@Configuration
@ComponentScan(basePackages = "br.com.squamata")
@EnableAutoConfiguration
@EnableEntityLinks
@EnableMongoRepositories(basePackages = "br.com.squamata.spending.repository")
@EnableSpringConfigured
@EnableAspectJAutoProxy
@EnableWebSecurity
@Import({HypermediaConfiguration.class, MongoConfig.class})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}