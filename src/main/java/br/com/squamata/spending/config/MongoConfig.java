package br.com.squamata.spending.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration {

    @Value("${dataBaseName}")
    private String dataBaseName;
    @Value("${mongoHost}")
    private String mongoHost;
    @Value("${mongoPort}")
    private int mongoPort;

    public MongoConfig() {
    }

    @Override
    public String getDatabaseName() {
        return dataBaseName;
    }

    @Override
    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient(mongoHost, mongoPort);
    }

    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {
        return new SimpleMongoDbFactory(new MongoClient(), getDatabaseName());
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
        return mongoTemplate;

    }
}