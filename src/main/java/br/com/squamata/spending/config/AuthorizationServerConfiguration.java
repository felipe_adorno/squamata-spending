package br.com.squamata.spending.config;

import br.com.squamata.spending.spring.security.AuthenticationProvider;
import br.com.squamata.spending.spring.security.ClientDatailServiceProvider;
import br.com.squamata.spending.spring.security.entrypoint.RestAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthorizationServerTokenServices squamataTokenService;
    @Autowired
    private ObjectPostProcessor<Object> autowireBeanFactoryObjectPostProcessor;
    @Autowired
    private ClientDatailServiceProvider clientDatailServiceProvider;
    @Autowired
    private AuthenticationProvider authenticationProvider;
    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.authenticationEntryPoint(restAuthenticationEntryPoint).allowFormAuthenticationForClients();
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.tokenServices(squamataTokenService).authenticationManager(authManager());
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.withClientDetails(clientDatailServiceProvider);
    }

    @Bean(name = {"authManager", BeanIds.AUTHENTICATION_MANAGER})
    public AuthenticationManager authManager() throws Exception {
        return new AuthenticationManagerBuilder(autowireBeanFactoryObjectPostProcessor).authenticationProvider(authenticationProvider).build();
    }
}