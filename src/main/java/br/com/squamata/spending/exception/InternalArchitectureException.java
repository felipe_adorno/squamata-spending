package br.com.squamata.spending.exception;

import br.com.squamata.spending.model.error.MessageType;

/**
 * The Class InternalArchitectureException.
 *
 * @author cad_fadorno
 */
public class InternalArchitectureException extends RuntimeException {
    public InternalArchitectureException() {
        super(MessageType.Internal_Architecture_Error.getDescription());
    }
    public InternalArchitectureException(String message) {
        super(message);
    }
}
