package br.com.squamata.spending.spring.security;

import br.com.squamata.spending.model.ClientAPP;
import br.com.squamata.spending.repository.ClientAPPRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Component
public class ClientDatailServiceProvider implements ClientDetailsService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${resourceId}")
    private String resourceId;

    @Autowired
    private ClientAPPRepository clientAPPRepository;

    @Override
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        try {
            ClientAPP apiClientDocument = clientAPPRepository.findOne(clientId);
            if (apiClientDocument == null) {
                throw new ClientRegistrationException("Client not exist");
            }
            BaseClientDetails baseClientDetails = new BaseClientDetails();
            baseClientDetails.setClientId(apiClientDocument.getId());
            baseClientDetails.setClientSecret(apiClientDocument.getClientSecret());
            baseClientDetails.setAuthorizedGrantTypes(apiClientDocument.getAuthorizedGrantTypes());
            baseClientDetails.setAuthorities(AuthorityUtils.commaSeparatedStringToAuthorityList(apiClientDocument.getRolesCSV()));

            List<String> resources = new ArrayList<String>();
            resources.add(resourceId);
            baseClientDetails.setResourceIds(resources);

            return baseClientDetails;

        } catch (Exception exception) {
            logger.error("exception on login " + exception);
            throw new ClientRegistrationException("Client not exist");
        }
    }
}
