package br.com.squamata.spending.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.stereotype.Service;

/**
* The Class SquamataTokenService.
*
* @author cad_fadorno
*/
@Service
public class SquamataTokenService extends DefaultTokenServices {

    private TokenStore tokenStore = new InMemoryTokenStore();
    @Autowired
    private ClientDatailServiceProvider clientDatailServiceProvider;

    public SquamataTokenService() {
        this.setTokenStore(tokenStore);
        this.setClientDetailsService(clientDatailServiceProvider);
        this.setSupportRefreshToken(true);
    }
}
