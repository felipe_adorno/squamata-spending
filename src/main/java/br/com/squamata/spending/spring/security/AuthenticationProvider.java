package br.com.squamata.spending.spring.security;

import br.com.squamata.spending.model.User;
import br.com.squamata.spending.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Component
public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserRepository userRepository;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    public UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        try {
            String password = (String) authentication.getCredentials();
            if (!StringUtils.hasText(password)) {
                logger.warn("Username {}: no password provided", username);
                throw new BadCredentialsException("Please enter password");
            }

            User user = userRepository.findByUserName(username);
            if (user == null) {
                logger.warn("Username {} password {}: resource not found", username, password);
                throw new UsernameNotFoundException("Invalid Login");
            }

            if (!new BCryptPasswordEncoder().matches(password, user.getPassword())) {
                logger.warn("Username {} password {}: invalid password", username, password);
                throw new BadCredentialsException("Invalid Login");
            }

//        if (!(UserAccountStatus.STATUS_APPROVED.name().equals(resource.getStatus()))) {
//        	logger.warn("Username {}: not approved", username);
//            throw new BadCredentialsException("User has not been approved");
//        }

            if (!user.isEnabled()) {
                logger.warn("Username {}: disabled", username);
                throw new BadCredentialsException("User disabled");
            }

            final List<GrantedAuthority> auths;
            if (!user.getRoles().isEmpty()) {
                auths = AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRolesCSV());
            } else {
                auths = AuthorityUtils.NO_AUTHORITIES;
            }

            return new org.springframework.security.core.userdetails.User(username, password, user.isEnabled(),
                    user.isAccountNonExpired(),
                    user.isCredentialsNonExpired(),
                    user.isAccountNonLocked(),
                    auths);

        } catch (Exception exception) {
            logger.error("exception on login " + exception);
            throw new UsernameNotFoundException("Invalid Login");
        }
    }

}