package br.com.squamata.spending.repository;

import br.com.squamata.spending.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * The Interface UserRepository.
 *
 * @author cad_fadorno
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {
    public User findByUserName(String username);
}
