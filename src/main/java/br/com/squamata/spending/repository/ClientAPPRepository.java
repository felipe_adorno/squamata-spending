package br.com.squamata.spending.repository;

import br.com.squamata.spending.model.ClientAPP;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Repository
public interface ClientAPPRepository extends PagingAndSortingRepository<ClientAPP, String> {
}
