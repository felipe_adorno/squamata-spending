package br.com.squamata.spending.aspect;

import br.com.squamata.spending.util.logging.FluentLogger;
import br.com.squamata.spending.util.logging.LoggerService;
import br.com.squamata.spending.util.logging.RecursiveLogKey;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.util.Arrays;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Aspect
public class ModelLayerLoggingAspect {

    private static final String EMPTY_STRING = "";
    private static final FluentLogger logger = LoggerService.init(ModelLayerLoggingAspect.class);

    @Around("@within(br.com.squamata.spending.annotation.ModelLayer) "
            + "&& execution(public * br.com.squamata.spending.service.model.v*..*.*(..))"
            + "&& !execution(public void set*(..))"
            + "&& !execution(public String toString(..))"
            + "&& !execution(public boolean equals(..))"
            + "&& !execution(public int hashCode(..))"
            + "&& !execution(public * get*(..))"
            + "&& !execution(public * is*(..))"
            + "&& !@annotation(br.com.squamata.spending.annotation.LoggingOff)")
    public Object aroundModelLayer(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        logger.all().logDebug("Around Model Layer " + proceedingJoinPoint);

        long startTime = System.currentTimeMillis();

        Object model = null;
        Object returnObject = null;
        String method = null;
        Object[] parameters = null;
        try {
            model = proceedingJoinPoint.getThis();
            method = proceedingJoinPoint.getSignature().getName();
            parameters = proceedingJoinPoint.getArgs();
            returnObject = proceedingJoinPoint.proceed();
            logExecutionAndParameters(model, method, parameters, executionTime(startTime));
        } catch (Exception ex) {
            logException(model, method, executionTime(startTime), ex);
            throw ex;
        }
        return returnObject;
    }

    private long executionTime(long startTime) {
        return (System.currentTimeMillis() - startTime);
    }

    private RecursiveLogKey logModel(Object model) {
        final String modelName = model.getClass().getSimpleName();
        return logger.all().logKey("model").value(modelName);
    }

    private void logParameters(final RecursiveLogKey logBuilder, final Object[] parameters) {
        if (parameters != null && parameters.length > 0) {
            logBuilder.logKey("params").value(Arrays.asList(parameters).toString());
        }
    }

    private void logExecutionAndParameters(Object model, String method, Object[] parameters, long executionTime) {
        try {
            RecursiveLogKey logBuilder = logModel(model).logKey("method").value(method);
            logParameters(logBuilder, parameters);
            logBuilder.logKey("msecs").value(executionTime);
            logBuilder.asInfo();
            logBuilder.logKey("state").value(String.valueOf(model)).asDebug();
        } catch (Exception ex) {
            logger.all()
                    .logKey("error").value("Error on LogExecution of ModelLayerLoggingAspect!")
                    .logKey("exception").value(ex)
                    .asError();
            logger.file().logError("Error on LogExecution of ModelLayerLoggingAspect!", ex);
        }
    }

    private void logException(Object model, String method, long executionTime, Exception ex) {
        try {
            RecursiveLogKey logBuilder = logModel(model)
                    .logKey("method").value(method)
                    .logKey("state").value(String.valueOf(model))
                    .logKey("msecs").value(executionTime);

            logBuilder.logKey("exception").value(ex).asError();
            logger.file().logError("", ex);
        } catch (Exception e) {
            logger.all().logKey("error").value("Error on LogException of ModelLayerLoggingAspect!")
                    .logKey("exception").value(e)
                    .logKey("originalException").value(ex)
                    .asError();
            logger.file().logError("Error on LogExecution of ModelLayerLoggingAspect!", ex);
            logger.file().logError("Original Exception", ex);
        }
    }
}
