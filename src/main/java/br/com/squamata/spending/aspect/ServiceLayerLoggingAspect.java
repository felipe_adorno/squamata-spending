package br.com.squamata.spending.aspect;

import br.com.squamata.spending.util.logging.FluentLogger;
import br.com.squamata.spending.util.logging.LoggerService;
import br.com.squamata.spending.util.logging.RecursiveLogKey;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.util.Arrays;

/**
* @author Felipe Adorno (felipeadsc@gmail.com)
*/
@Aspect
public class ServiceLayerLoggingAspect {

    private static final String EMPTY_STRING = "";
    private static final FluentLogger logger = LoggerService.init(ServiceLayerLoggingAspect.class);

    @Around("@within(br.com.squamata.spending.annotation.ServiceLayer) "
            + "&& execution(public * br.com.squamata.spending.service.provider..*.*(..))"
            + "&& !execution(public String toString(..))"
            + "&& !execution(void set*(..))"
            + "&& !@annotation(br.com.squamata.spending.annotation.LoggingOff)")
    public Object aroundServiceLayer(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        logger.all().logDebug("Logging ServiceLayer " + proceedingJoinPoint);

        long startTime = System.currentTimeMillis();

        Object service = null;
        Object[] parameters = null;
        Object returnObject = null;
        String method = null;
        try {
            service = proceedingJoinPoint.getThis();
            parameters = proceedingJoinPoint.getArgs();
            method = proceedingJoinPoint.getSignature().getName();
            logMethodAndParameters(service, method, parameters);
            returnObject = proceedingJoinPoint.proceed();
            logExecutionAndParametersAndReturn(service, method, executionTime(startTime), parameters, returnObject);
        } catch (Exception ex) {
            logException(service, method, parameters, executionTime(startTime), ex);
            throw ex;
        }
        return returnObject;
    }

    private long executionTime(long startTime) {
        return (System.currentTimeMillis() - startTime);
    }

    private void logMethodAndParameters(Object service, String method, Object[] parameters) {
        try {
            RecursiveLogKey logBuilder = logServiceAndMethod(service, method);
            logParameters(logBuilder, parameters);
            logBuilder.asDebug();
        } catch (Exception ex) {
            logAspectException(service, "Error on logMethodAndParameters of ServiceLayerLoggingAspect!", ex);
        }
    }

    private void logExecutionAndParametersAndReturn(Object service, String method, long executionTime, Object[] parameters, Object returnObject) {
        try {
            RecursiveLogKey logBuilder = logServiceAndMethodAndTime(service, method, executionTime);
            logParameters(logBuilder, parameters);
            logBuilder.asInfo();
        } catch (Exception ex) {
            logAspectException(service, "Error on logExecution of ServiceLayerLoggingAspect!", ex);
        }
    }

    private RecursiveLogKey logServiceAndMethodAndTime(Object service, String method, long executionTime) {
        return logServiceAndMethod(service, method).logKey("msecs").value(executionTime);
    }

    private void logParameters(final RecursiveLogKey logBuilder, final Object[] parameters) {
        if (parameters != null && parameters.length > 0) {
            logBuilder.logKey("params").value(Arrays.asList(parameters).toString());
        }
    }

    private RecursiveLogKey logServiceAndMethod(Object service, String method) {
        return logService(service).logKey("method").value(method);
    }

    private RecursiveLogKey logService(Object service) {
        final String serviceName = service.getClass().getSimpleName();
        return logger.all().logKey("service").value(serviceName);
    }

    private void logException(RecursiveLogKey logBuilder, Exception ex) {
        logBuilder.logKey("exception").value(ex).asError();
        logger.file().logError(EMPTY_STRING, ex);
    }

    private void logAspectException(Object service, String message, Exception ex) {
        logService(service)
                .logKey("error").value(message)
                .logKey("exception").value(ex)
                .asError();
        logger.file().logError(message, ex);
    }

    private void logException(Object service, String method, Object[] parameters, long executionTime, Exception ex) {
        try {
            RecursiveLogKey logBuilder = logServiceAndMethodAndTime(service, method, executionTime);
            logParameters(logBuilder, parameters);
            logException(logBuilder, ex);
            logBuilder.asError();
        } catch (Exception e) {
            logAspectException(service, "Error on logException of ServiceLayerLoggingAspect!", e);
            logger.file().logError("Original Exception", ex);
        }
    }
}
