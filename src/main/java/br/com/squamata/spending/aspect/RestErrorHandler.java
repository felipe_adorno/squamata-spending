package br.com.squamata.spending.aspect;

import br.com.squamata.spending.annotation.RestControllerAdvice;
import br.com.squamata.spending.model.error.Message;
import br.com.squamata.spending.model.error.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import java.util.List;
import java.util.Locale;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@RestControllerAdvice
public class RestErrorHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private MessageSource messageSource;

    @Autowired
    public RestErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<String> handlerValidationException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        Message message = processFieldErrors(fieldErrors);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=utf-8");

        return new ResponseEntity<>(message.toJson(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler ({ServletRequestBindingException.class,
                        TypeMismatchException.class,
                        HttpMessageNotReadableException.class,
                        MissingServletRequestPartException.class})
    @ResponseStatus (HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<String> handlerMissingServletRequestParameterException(Exception ex) {

        Message message = new Message(MessageType.Parameter_Error, messageSource.getMessage("invalid.request", null, null));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<>(message.toJson(), responseHeaders, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler ({HttpMediaTypeNotSupportedException.class})
    @ResponseStatus (HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ResponseBody
    public ResponseEntity<String> handlerHttpMediaTypeNotSupportedException(Exception ex) {

        Message message = new Message(MessageType.Unsuported_MediaType_Error, messageSource.getMessage("invalid.MediaType", null, null));
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "application/json; charset=utf-8");
        return new ResponseEntity<>(message.toJson(), responseHeaders, HttpStatus.UNSUPPORTED_MEDIA_TYPE);

    }


    private Message processFieldErrors(List<FieldError> fieldErrors) {
        Message message = new Message(MessageType.Parameter_Error, messageSource.getMessage("invalid.request", null, null));
        for (FieldError fieldError: fieldErrors) {
            String localizedErrorMessage = resolveLocalizedErrorMessage(fieldError);
            message.addNotification(localizedErrorMessage);
        }
        return  message;
    }

    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        Locale currentLocale =  LocaleContextHolder.getLocale();
        String localizedErrorMessage = messageSource.getMessage(fieldError, currentLocale);
        if (localizedErrorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            localizedErrorMessage = fieldErrorCodes[0];
        }
        return localizedErrorMessage;
    }
}