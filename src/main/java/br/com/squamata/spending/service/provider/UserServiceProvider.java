package br.com.squamata.spending.service.provider;

import br.com.squamata.spending.annotation.ServiceLayer;
import br.com.squamata.spending.service.UserService;
import br.com.squamata.spending.service.model.UserModel;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@ServiceLayer
public class UserServiceProvider implements UserService {

    @Override
    public UserModel persist(UserModel userDomainModel) {
        return userDomainModel.persist();
    }

    @Override
    public UserModel fetchByUserName(UserModel userModel) {
        return userModel.fetchByUserName();
    }

    @Override
    public UserModel fetchById(UserModel userModel) {
        return userModel.fetchById();
    }
}
