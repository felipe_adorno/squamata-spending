package br.com.squamata.spending.service.model;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public interface UserModel {
    UserModel persist();

    UserModel fetchByUserName();

    UserModel fetchById();
}
