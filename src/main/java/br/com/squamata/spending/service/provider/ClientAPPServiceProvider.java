package br.com.squamata.spending.service.provider;

import br.com.squamata.spending.annotation.ServiceLayer;
import br.com.squamata.spending.service.ClientAPPService;
import br.com.squamata.spending.service.model.ClientAPPModel;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@ServiceLayer
public class ClientAPPServiceProvider implements ClientAPPService {

    @Override
    public ClientAPPModel persist(final ClientAPPModel clientAPPModel) {
        return clientAPPModel.persist();
    }

    public ClientAPPModel fetchById(final ClientAPPModel clientAPPModel) {
        return clientAPPModel.fetchById();
    }
}
