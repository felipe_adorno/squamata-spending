package br.com.squamata.spending.service.model.v1;

import br.com.squamata.spending.annotation.ModelLayer;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.model.v1.mapper.UserMapper;
import br.com.squamata.spending.repository.UserRepository;
import br.com.squamata.spending.service.model.UserModel;
import br.com.squamata.spending.util.tostring.ToStringBuilder;
import br.com.squamata.spending.util.tostring.annotation.ToStringExclude;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static br.com.squamata.spending.util.ValidateUtils.notNullParameter;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@ModelLayer
@Configurable
public class UserDomainModel implements UserModel {

    private String id;
    private String name;
    private String phone;
    private String userName;
    private Date insertDate;
    @ToStringExclude
    private String password;
    private boolean enabled;
    private boolean accountNonExpired;
    private boolean credentialsNonExpired;
    private boolean accountNonLocked;
    private List<String> roles;

    @ToStringExclude
    @Autowired
    private UserRepository userRepository;
    @ToStringExclude
    @Autowired
    private UserMapper userMapper;

    public UserDomainModel() {
    }

    public UserDomainModel(String name, String phone, String userName, String password) {
        this.name = name;
        this.phone = phone;
        this.userName = userName;
        this.password = password;
    }

    @Override
    public UserModel persist() {
        notNullParameter(getName(), "Name");
        notNullParameter(getPhone(), "Phone");
        notNullParameter(getUserName(), "Username");
        notNullParameter(getPassword(), "Password");
        addRoleUserIfNoRoles();
        return userMapper.mapDomainModel(userRepository.save(userMapper.mapModel(this)));
    }

    @Override
    public UserModel fetchByUserName() {
        //TODO: TRATAR EXCEPTION AQUI
        notNullParameter(getUserName(), "User Name");
        return userMapper.mapDomainModel(userRepository.findByUserName(getUserName()));
    }

    @Override
    public UserModel fetchById() {
        //TODO: TRATAR EXCEPTION AQUI
        notNullParameter(getId(), "Id");
        return userMapper.mapDomainModel(userRepository.findOne(getId()));
    }

    private void addRoleUserIfNoRoles() {
        if (CollectionUtils.isEmpty(getRoles())) {
            getRoles().add(Role.ROLE_USER.getRole());
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public List<String> getRoles() {
        if (roles == null) {
            roles = new ArrayList<String>();
        }
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDomainModel that = (UserDomainModel) o;

        if (accountNonExpired != that.accountNonExpired) return false;
        if (accountNonLocked != that.accountNonLocked) return false;
        if (credentialsNonExpired != that.credentialsNonExpired) return false;
        if (enabled != that.enabled) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (insertDate != null ? !insertDate.equals(that.insertDate) : that.insertDate != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (phone != null ? !phone.equals(that.phone) : that.phone != null) return false;
        if (roles != null ? !roles.equals(that.roles) : that.roles != null) return false;
        if (userMapper != null ? !userMapper.equals(that.userMapper) : that.userMapper != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
        if (userRepository != null ? !userRepository.equals(that.userRepository) : that.userRepository != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (insertDate != null ? insertDate.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (accountNonExpired ? 1 : 0);
        result = 31 * result + (credentialsNonExpired ? 1 : 0);
        result = 31 * result + (accountNonLocked ? 1 : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        result = 31 * result + (userRepository != null ? userRepository.hashCode() : 0);
        result = 31 * result + (userMapper != null ? userMapper.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
