package br.com.squamata.spending.service;

import br.com.squamata.spending.service.model.UserModel;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public interface UserService {
    UserModel persist(final UserModel userModel);

    UserModel fetchByUserName(final UserModel userModel);

    UserModel fetchById(final UserModel userModel);
}
