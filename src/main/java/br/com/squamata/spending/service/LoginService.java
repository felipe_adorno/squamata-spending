package br.com.squamata.spending.service;

import br.com.squamata.spending.model.User;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public interface LoginService {
    public User findUserByUserName(final String username);
}
