package br.com.squamata.spending.service.model.v1;

import br.com.squamata.spending.annotation.ModelLayer;
import br.com.squamata.spending.model.AuthorizedGrantType;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.model.v1.mapper.ClientAPPMapper;
import br.com.squamata.spending.repository.ClientAPPRepository;
import br.com.squamata.spending.service.model.ClientAPPModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static br.com.squamata.spending.util.ValidateUtils.notNullParameter;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@ModelLayer
@Configurable
public class ClientAPPDomainModel implements ClientAPPModel {

    private String id;
    private String appName;
    private String clientSecret;
    private List<String> authorizedGrantTypes;
    private List<String> roles;

    @Autowired
    private ClientAPPRepository clientAPPRepository;
    @Autowired
    private ClientAPPMapper clientAPPMapper;

    public ClientAPPDomainModel() {
    }

    public ClientAPPDomainModel(String appName) {
        this.appName = appName;
    }

    @Override
    public ClientAPPModel persist() {
        notNullParameter(getAppName(), "appName");
        addClintAPPDefaultAtributes();
        return clientAPPMapper.mapDomainModel(
                clientAPPRepository.save(clientAPPMapper.mapModel(this)));
    }

    @Override
    public ClientAPPModel fetchById() {
        notNullParameter(getId(), "id");
        return clientAPPMapper.mapDomainModel(clientAPPRepository.findOne(getId()));
    }

    private void addClintAPPDefaultAtributes() {
        setClientSecret(UUID.randomUUID().toString());
        getRoles().add(Role.ROLE_USER.getRole());
        getAuthorizedGrantTypes().add(AuthorizedGrantType.PASSWORD.getAuthorizedGrantType());
        getAuthorizedGrantTypes().add(AuthorizedGrantType.REFRESH_TOKEN.getAuthorizedGrantType());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public List<String> getAuthorizedGrantTypes() {
        if (CollectionUtils.isEmpty(authorizedGrantTypes)) {
            authorizedGrantTypes = new ArrayList<String>();
        }
        return authorizedGrantTypes;
    }

    public void setAuthorizedGrantTypes(List<String> authorizedGrantTypes) {
        this.authorizedGrantTypes = authorizedGrantTypes;
    }

    public List<String> getRoles() {
        if (CollectionUtils.isEmpty(roles)) {
            roles = new ArrayList<String>();
        }
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public void setClientAPPRepository(ClientAPPRepository clientAPPRepository) {
        this.clientAPPRepository = clientAPPRepository;
    }

    public void setClientAPPMapper(ClientAPPMapper clientAPPMapper) {
        this.clientAPPMapper = clientAPPMapper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientAPPDomainModel that = (ClientAPPDomainModel) o;

        if (appName != null ? !appName.equals(that.appName) : that.appName != null) return false;
        if (authorizedGrantTypes != null ? !authorizedGrantTypes.equals(that.authorizedGrantTypes) : that.authorizedGrantTypes != null)
            return false;
        if (clientSecret != null ? !clientSecret.equals(that.clientSecret) : that.clientSecret != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (roles != null ? !roles.equals(that.roles) : that.roles != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (appName != null ? appName.hashCode() : 0);
        result = 31 * result + (clientSecret != null ? clientSecret.hashCode() : 0);
        result = 31 * result + (authorizedGrantTypes != null ? authorizedGrantTypes.hashCode() : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        return result;
    }
}
