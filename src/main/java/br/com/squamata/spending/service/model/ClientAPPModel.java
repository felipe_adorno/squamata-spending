package br.com.squamata.spending.service.model;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public interface ClientAPPModel {
    ClientAPPModel persist();

    ClientAPPModel fetchById();
}
