package br.com.squamata.spending.service;

import br.com.squamata.spending.service.model.ClientAPPModel;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public interface ClientAPPService {
    ClientAPPModel persist(final ClientAPPModel clientAPPModel);

    ClientAPPModel fetchById(ClientAPPModel clientAPPModel);
}
