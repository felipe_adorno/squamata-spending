package br.com.squamata.spending.model;

import br.com.squamata.spending.util.tostring.ToStringBuilder;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public enum Role {

    ROLE_USER("ROLE_USER"),
    ROLE_API("ROLE_API");

    private String role;

    private Role(String role) {
        this.role = role;
    }

    public static Role getByKey(String key) {
        for (Role role : Role.values()) {
            if (role.getRole().equals(key)) {
                return role;
            }
        }
        return null;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}