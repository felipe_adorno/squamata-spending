package br.com.squamata.spending.model.v1.mapper;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public interface ModelMapper<O, I> {
    O mapModel(I i);

    I mapDomainModel(O i);
}
