package br.com.squamata.spending.model.v1.mapper;

import br.com.squamata.spending.annotation.Mapper;
import br.com.squamata.spending.model.ClientAPP;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.service.model.v1.ClientAPPDomainModel;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Mapper
public class ClientAPPMapper implements ModelMapper<ClientAPP, ClientAPPDomainModel> {

    @Override
    public ClientAPPDomainModel mapDomainModel(ClientAPP clientAPP) {
        ClientAPPDomainModel clientAPPDomainModel = new ClientAPPDomainModel(clientAPP.getAppName());
        clientAPPDomainModel.setClientSecret(clientAPP.getClientSecret());
        clientAPPDomainModel.setAuthorizedGrantTypes(clientAPP.getAuthorizedGrantTypes());
        clientAPPDomainModel.setId(clientAPP.getId());
        if (!CollectionUtils.isEmpty(clientAPP.getRoles())) {
            clientAPPDomainModel.setRoles(mapRolesToString(clientAPP.getRoles()));
        }
        return clientAPPDomainModel;
    }

    @Override
    public ClientAPP mapModel(ClientAPPDomainModel clientAPPDomainModel) {
        ClientAPP clientAPP = new ClientAPP();
        clientAPP.setAppName(clientAPPDomainModel.getAppName());
        clientAPP.setClientSecret(clientAPPDomainModel.getClientSecret());
        clientAPP.setAuthorizedGrantTypes(clientAPPDomainModel.getAuthorizedGrantTypes());
        if (!CollectionUtils.isEmpty(clientAPPDomainModel.getRoles())) {
            clientAPP.setRoles(mapRoles(clientAPPDomainModel.getRoles()));
        }
        return clientAPP;
    }

    private List<String> mapRolesToString(List<Role> roles) {
        List<String> rolesReturn = new ArrayList<String>();
        for (Role role : roles) {
            rolesReturn.add(role.getRole());
        }
        return rolesReturn;
    }

    private List<Role> mapRoles(List<String> roles) {
        List<Role> rolesReturn = new ArrayList<Role>();
        for (String role : roles) {
            rolesReturn.add(Role.getByKey(role));
        }
        return rolesReturn;
    }

}
