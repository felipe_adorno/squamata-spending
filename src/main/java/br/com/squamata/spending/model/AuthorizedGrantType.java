package br.com.squamata.spending.model;

/**
 * The Class AuthorizedGrantType.
 *
 * @author cad_fadorno
 */
public enum AuthorizedGrantType {
    PASSWORD("password"),
    REFRESH_TOKEN("refresh_token");

    private String authorizedGrantType;

    private AuthorizedGrantType(String authorizedGrantType) {
        this.authorizedGrantType = authorizedGrantType;
    }

    public static AuthorizedGrantType getByKey(String key) {
        for (AuthorizedGrantType authorizedGrantType : AuthorizedGrantType.values()) {
            if (authorizedGrantType.getAuthorizedGrantType().equals(key)) {
                return authorizedGrantType;
            }
        }
        return null;
    }

    public String getAuthorizedGrantType() {
        return authorizedGrantType;
    }
}
