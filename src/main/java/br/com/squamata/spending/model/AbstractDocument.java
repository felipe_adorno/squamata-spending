package br.com.squamata.spending.model;

import org.springframework.data.annotation.Id;

import java.util.UUID;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
public class AbstractDocument {

    @Id
    private String id;

    public AbstractDocument() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (this.id == null || obj == null || !(this.getClass().equals(obj.getClass()))) {
            return false;
        }

        AbstractDocument that = (AbstractDocument) obj;

        return this.id.equals(that.getId());
    }

    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }
}