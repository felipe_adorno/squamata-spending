package br.com.squamata.spending.model.v1.mapper;

import br.com.squamata.spending.annotation.Mapper;
import br.com.squamata.spending.model.Role;
import br.com.squamata.spending.model.User;
import br.com.squamata.spending.service.model.v1.UserDomainModel;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import static br.com.squamata.spending.util.ValidateUtils.notNullParameter;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Mapper
public class UserMapper implements ModelMapper<User, UserDomainModel> {

    @Override
    public UserDomainModel mapDomainModel(User user) {
        UserDomainModel userDomainModel = new UserDomainModel(user.getName(), user.getPhone(), user.getUserName(), user.getPassword());
        userDomainModel.setInsertDate(user.getInsertDate());
        userDomainModel.setId(user.getId());
        userDomainModel.setEnabled(user.isEnabled());
        userDomainModel.setAccountNonExpired(user.isAccountNonExpired());
        userDomainModel.setCredentialsNonExpired(user.isCredentialsNonExpired());
        userDomainModel.setAccountNonLocked(user.isAccountNonLocked());
        if (!CollectionUtils.isEmpty(user.getRoles())) {
            userDomainModel.setRoles(mapRolesToString(user.getRoles()));
        }
        return userDomainModel;
    }

    @Override
    public User mapModel(UserDomainModel userDomainModel) {
        User userDocument = new User();
        userDocument.setName(userDomainModel.getName());
        userDocument.setPhone(userDomainModel.getPhone());
        userDocument.setUserName(userDomainModel.getUserName());
        userDocument.setInsertDate(userDomainModel.getInsertDate());
        userDocument.setPassword(encryptPassword(userDomainModel.getPassword()));
        userDocument.setEnabled(userDomainModel.isEnabled());
        userDocument.setAccountNonExpired(userDomainModel.isAccountNonExpired());
        userDocument.setCredentialsNonExpired(userDomainModel.isCredentialsNonExpired());
        userDocument.setAccountNonLocked(userDomainModel.isAccountNonLocked());
        if (!CollectionUtils.isEmpty(userDomainModel.getRoles())) {
            userDocument.setRoles(mapRoles(userDomainModel.getRoles()));
        }
        return userDocument;
    }

    private List<String> mapRolesToString(List<Role> roles) {
        List<String> rolesReturn = new ArrayList<String>();
        for (Role role : roles) {
            rolesReturn.add(role.getRole());
        }
        return rolesReturn;
    }

    private List<Role> mapRoles(List<String> roles) {
        List<Role> rolesReturn = new ArrayList<Role>();
        for (String role : roles) {
            rolesReturn.add(Role.getByKey(role));
        }
        return rolesReturn;
    }

    private String encryptPassword(String password) {
        notNullParameter(password, "Password");
        return new BCryptPasswordEncoder().encode(password);
    }
}
