package br.com.squamata.spending.model;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Iterator;
import java.util.List;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@Document(collection = "apiClientDocument")
public class ClientAPP extends AbstractDocument {

    private String appName;
    private String clientSecret;
    private List<String> authorizedGrantTypes;
    private List<Role> roles;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public List<String> getAuthorizedGrantTypes() {
        return authorizedGrantTypes;
    }

    public void setAuthorizedGrantTypes(List<String> authorizedGrantTypes) {
        this.authorizedGrantTypes = authorizedGrantTypes;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getRolesCSV() {
        StringBuilder sb = new StringBuilder();
        for (Iterator<Role> iter = this.roles.iterator(); iter.hasNext(); ) {
            sb.append(iter.next().getRole());
            if (iter.hasNext()) {
                sb.append(',');
            }
        }
        return sb.toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ClientAPP clientAPP = (ClientAPP) o;

        if (appName != null ? !appName.equals(clientAPP.appName) : clientAPP.appName != null) return false;
        if (authorizedGrantTypes != null ? !authorizedGrantTypes.equals(clientAPP.authorizedGrantTypes) : clientAPP.authorizedGrantTypes != null)
            return false;
        if (clientSecret != null ? !clientSecret.equals(clientAPP.clientSecret) : clientAPP.clientSecret != null)
            return false;
        if (roles != null ? !roles.equals(clientAPP.roles) : clientAPP.roles != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (appName != null ? appName.hashCode() : 0);
        result = 31 * result + (clientSecret != null ? clientSecret.hashCode() : 0);
        result = 31 * result + (authorizedGrantTypes != null ? authorizedGrantTypes.hashCode() : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        return result;
    }
}
