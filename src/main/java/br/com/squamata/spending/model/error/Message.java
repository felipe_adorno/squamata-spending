package br.com.squamata.spending.model.error;

import br.com.squamata.spending.util.DateTimeJsonSerializer;
import br.com.squamata.spending.util.ValidateUtils;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.xml.bind.annotation.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Adorno (felipeadsc@gmail.com)
 */
@XmlRootElement(name = "Message")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"code",
        "type",
        "description",
        "httpReason",
        "date",
        "notifications"})
@JsonPropertyOrder({"code",
        "type",
        "description",
        "httpReason",
        "date",
        "notifications"})
public class Message {

    private static final String MESSAGE_TYPE = "type";
    private static final int CLIENT_ERRO = 4 ;
    private static final int BASE = 100;

    @XmlElement(required = true)
    private MessageType type;
    @XmlElement
    private String description;
    @XmlElement
    @JsonSerialize(using = DateTimeJsonSerializer.class)
    private Date date = new Date();
    @XmlElement
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
    private List<String> notifications = new ArrayList<>();

    public Message() {
    }

    public Message(int code, MessageType type) {
        super();
        ValidateUtils.notNullParameter(type, MESSAGE_TYPE);
        this.type = type;
    }

    public Message(MessageType type) {
        super();
        ValidateUtils.notNullParameter(type, MESSAGE_TYPE);
        this.type = type;
        this.description = type.getDescription();
    }

    public Message(MessageType type, String description) {
        super();
        ValidateUtils.notNullParameter(type, MESSAGE_TYPE);
        ValidateUtils.notNullParameter(description, "description");
        this.type = type;
        this.description = description;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<String> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<String> notifications) {
        this.notifications = notifications;
    }

    public Message addNotification(String notification) {
        getNotifications().add(notification);
        return this;
    }

    public String toJson() {
        String mappedObject = "";
        ObjectMapper mapper = new ObjectMapper();
        try {
            mappedObject = mapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mappedObject;
    }
}
